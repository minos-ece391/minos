#include "paging.h"

static uint32_t page_directory[1024] __attribute__((aligned(4096)));
static uint32_t first_page_table[1024] __attribute__((aligned(4096)));
static uint32_t video_page_table[1024] __attribute__((aligned(4096)));

/***** Paging functions *****/

/* 
 * initialize_paging
 *   DESCRIPTION: Initialize paging, set the IDT entry for page fault interrupt handler and enable paging.
 *   INPUTS: none
 *   OUTPUTS: Initializes the page directory. Sets IDT[14] to the page fault handler. Enables paging.
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none 
 * 	 WARNING: 	   
 */
void initialize_paging()
{	
	//null pointer page
	first_page_table[0] = 0x0;
	
	// holds the physical address where we want to start mapping these pages to.
	// in this case, we want to map these pages to the very beginning of memory.
	//we will fill all 1024 entries in the table, mapping 4 megabytes
	int i;
	for(i = 1; i < 1024; i++)
	{
		// As the address is page aligned, it will always leave 12 bits zeroed.
		// Those bits are used by the attributes ;)
		first_page_table[i] = (i * 0x1000) | 3; // attributes: supervisor level, read/write, present.
	}

	//set each entry to not present
	for(i = 2; i < 1024; i++)
	{
		// This sets the following flags to the pages:
		//   Supervisor: Only kernel-mode can access them
		//   Write Enabled: It can be both read from and written to
		//   Not Present: The page table is not present
		page_directory[i] = (i * 0x400000) | 2;
	}

	for(i = 0; i < 1024; i++)
	{
		// As the address is page aligned, it will always leave 12 bits zeroed.
		// Those bits are used by the attributes ;)
		video_page_table[i] = (i * 0x1000) | 6; // attributes: supervisor level, read/write, present.
	}
	
	// attributes: supervisor level, read/write, present
	page_directory[0] = ((unsigned int)first_page_table) | 3;
	page_directory[1] = 0x400000 | 0x83;
	
   // Before we enable paging, we must register our page fault handler.
   //register_interrupt_handler(14, page_fault);
   SET_IDT_ENTRY(idt[14], page_fault);
   // Now, enable paging!
   enable_page();
}

/* 
 * enable_page
 *   DESCRIPTION: Enables paging by loading the address of the page directory in CR3 and setting the paging bit of CR0.
 *   INPUTS: none
 *   OUTPUTS: Enables paging. Sets CR3 to the address of the page directory.
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Paging is now enabled.
 * 	 WARNING: none   
 */
void enable_page()
{
   asm volatile("mov %0, %%cr3":: "r"(page_directory));		// we enable paging by loading cr3 with the address of the page directory 
   uint32_t cr4;
   asm volatile("mov %%cr4, %0": "=r"(cr4));
   cr4 |= 0x00000010; // Enable paging!
   asm volatile("mov %0, %%cr4":: "r"(cr4));
   uint32_t cr0;
   asm volatile("mov %%cr0, %0": "=r"(cr0));
   cr0 |= 0x80000000; // Enable paging!
   asm volatile("mov %0, %%cr0":: "r"(cr0));
}

/*
 * setpde
 *   DESCRIPTION: set certain page directory entry to the value passed in
 *   INPUTS: index --- the index of page directory entry
 *           value --- the value to set
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none.
 *      WARNING: none
 */
void setpde(uint32_t index, uint32_t value)
{
	if (index < 0x1000)
	{
		page_directory[index] = value;
	}
	// invalidate the tlb
	asm volatile(
		"movl 	%%cr3, %%eax\n\t"
		"movl 	%%eax, %%cr3\n\t"
		: : : "eax");
}

/*
 * getpde
 *   DESCRIPTION: get the page directory entry
 *   INPUTS: index --- the index of pde
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 *      WARNING: none
 */
uint32_t getpde(uint32_t index)
{
    return page_directory[index];
}

/* 
 * video_map_4kb
 *   DESCRIPTION: Map a 4kb page in the user space to the video memory
 *   INPUTS: p_addr - physcial address
 * 			v_addr - virtual address
 *			idx - terminal number
 *   OUTPUTS: Enable 4kb page
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 * 	 WARNING: none   
 */

void video_map_4kb(uint32_t p_addr, uint32_t v_addr, uint32_t idx)
{
	video_page_table[idx] = p_addr | 7;
	page_directory[v_addr >> 22] = ((unsigned int)video_page_table) | 7;

	// invalidate the tlb
	asm volatile(
		"movl 	%%cr3, %%eax\n\t"
		"movl 	%%eax, %%cr3\n\t"
		: : : "eax");
}


/* 
 * page_fault 
 *   DESCRIPTION: Page fault interrupt handler.
 *   INPUTS: none
 *   OUTPUTS: In the event of a page fault, the hander is executed and the address of the page fault is printed to the console.
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 * 	 WARNING: 	   
 */
void page_fault()
{
	uint32_t addr;
	pcb_t* cur_pcb;
	GET_CUR_PCB(cur_pcb);
	asm volatile ("mov %%cr2, %0" : "=r" (addr));
	printf("PAGE FAULT at 0x%x, pid = %d, parent = 0x%x\n", addr, cur_pcb->pid, cur_pcb->parent);
	halt(255);
}
