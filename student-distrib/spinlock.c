/** Functions for the spinlock. When on uniprocessor, the lock takes no effect. Use 
 *  lock with irq save.
*/

#include "spinlock.h"


/* 
 * spinlock_lock
 *   DESCRIPTION: Lock the given lock, without disabling the interrupt
 *   INPUTS: lock -- The lock to be locked 
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: When on uniprocessor, this function takes no effect
 *				   use spinlock_lock_save_irq when creating critical section
 * 	 WARNING: 	   Change the macro MULTIPROCESSOR when on different kinds of processor.
 *				   And the interrupt is not disabled
 *	 DEBUGING INFO: Haven't implemented yet
 */
void spinlock_lock(spinlock_t* lock)
{
#if MULTIPROCESSOR
	asm volatile("movl $1, %eax \n\t"
				 "lock_loop:\n\t"
				 "xchgl (%0), %eax \n\t"
				 "cmpl  %eax, $0 \n\t"
				 "jnz lock_loop \n\t"
				 : "r" (lock));
#endif
}

/* 
 * spinlock_lock_save_irq
 *   DESCRIPTION: Disable the interrupt, save the flag, and lock the lock
 *   INPUTS: lock -- The lock to be locked
 *		     flags - The flags
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Disable the interrupt
 * 	 WARNING: 	   Change the macro MULTIPROCESSOR when on different kinds of processor.
 */
void spinlock_lock_save_irq(spinlock_t* lock, uint32_t flags)
{
	cli_and_save(flags);					// save the flags
	spinlock_lock(lock);					// acqure the lock
}

/* 
 * spinlock_unlock
 *   DESCRIPTION: Unlock the spin lock, without enabling the interrupt
 *   INPUTS: lock -- The lock to be unlocked
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 * 	 WARNING: 	   No spining is implemented. Only be called by the owner of the lock
 *				   at this time
 */
void spinlock_unlock(spinlock_t* lock)
{
	lock->lock = 0;
}

/* 
 * spinlock_unlock_restore_irq
 *   DESCRIPTION: Unlock the spin lock, restore the flags
 *   INPUTS: lock -- The lock to be unlocked
 			 flags - The flags
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Retore the flags. Not garenteed to enable the interrupt
 * 	 WARNING: 	   No spining is implemented. Only be called by the owner of the lock
 *				   at this time
 */
void spinlock_unlock_restore_irq(spinlock_t* lock, uint32_t flags)
{
	spinlock_unlock(lock);
	restore_flags(flags);
}
