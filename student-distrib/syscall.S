
#define _ASM 1
#include "syscall_handle.h"

.globl handle_syscall
	.type	handle_syscall, @function

# The system call generic handler
# ** Parameters are passed through register
# EAX -- The syscall number
# EBX -- First param
# ECX -- Second param
# EDX -- Third param
# 

handle_syscall:
# set up the stack #
	pushl	%ebp
	movl	%esp, %ebp
	pushl 	%esi
	subl	$20, %esp
	leal	-20(%ebp), %esi

# load the parameter #
	pushal 
	movl 	%eax, 0(%esi) 
	movl 	%ebx, 4(%esi) 
	movl 	%ecx, 8(%esi) 
	movl 	%edx, 12(%esi)

# check parameter
	subl	$1, 0(%esi)
	cmpl 	$10, 0(%esi)				# check whether the syscall number is valid
	jae 	invalid_ard					# invalid argument in EAX
	movl	0(%esi), %eax
	movl	call_table(,%eax,8), %ecx 	# load the function address into ECX
	movl	call_table+4(,%eax,8), %edx # load the number of parameters to EDX
	movl 	$3, %edx
	movl	%esp, %ebx 					# temporarily save the current stack pointer to ebx
	pushl 	%esi
	pushl 	12(%esi)
	pushl 	8(%esi)
	pushl 	4(%esi)

# call the function
	call 	*%ecx
	addl 	$12, %esp 					# remove the parameters
	popl 	%esi
	movl 	%eax, -4(%esi) 				# save the return value
	jmp		return1

invalid_ard:
	movl	$-1, -4(%esi)

return1:
	popal								# pop all
	popl 	%eax 						# restore the return value
	addl	$16, %esp					# remove the local array
	popl 	%esi 						# retore the ESI
	leave
	iret

#undef _ASM
