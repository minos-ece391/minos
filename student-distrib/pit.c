#include "pit.h"

static void (*handler_func) ();

void init_pit(int hertz, void (*func) ())
{
    uint32_t flags;
    cli_and_save(flags);
    set_channel0_freq(hertz);
    handler_func = func;
    SET_IDT_ENTRY(idt[PIT_IDT_HANDLER], handle_pit);
    enable_irq(PIT_IRQ);
    restore_flags(flags);
}

void set_channel0_freq(int hertz)
{
    int divisor = DIV_CST / hertz;
    outb(0x36, PIT_CMD);
    outb((uint8_t)(divisor & 0xFF), PIT_CHNL0);
    outb((uint8_t)(divisor >> 8), PIT_CHNL0);
}

void set_channel2_freq(int hertz)
{
    int divisor = DIV_CST / hertz;
    outb(0xb6, PIT_CMD);
    outb((uint8_t)divisor, PIT_CHNL2);
    outb((uint8_t)(divisor >> 8), PIT_CHNL2);
}

void handle_pit()
{
    // uint32_t flags;
    cli();
    asm volatile("pusha");
    // cli_and_save(flags);
    send_eoi(PIT_IRQ);
    if (handler_func != NULL)
        handler_func();
    // restore_flags(flags);
    // deb_print();
    asm volatile("popa");
    asm volatile("leave");
    // deb_print();
    asm volatile("sti");
    // deb_print();
    asm volatile("iret");
}
