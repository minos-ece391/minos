#include "terminal.h"
#include "lib.h"
#include "paging.h"

static uint32_t video_buf[3] = {VIDEO_0, VIDEO_1, VIDEO_2};
terminal_info terminal[NUM_TERMINALS];	// each of the 3 entries corresponds to the terminal_info for a certain terminal


/* 
 * init_terminal
 *   DESCRIPTION: Initialize the terminals to a known initial state.
 *   INPUTS: none
 *   OUTPUTS: Terminal is cleared. Keyboard buffer is cleared. Cursor is at top left of terminal window. Active terminal set to id = 0. 
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Clears the keyboard buffer. Resets the x_pos and y_pos for each terminal. Sets the active terminal window to id = 0.
 * 	 WARNING: none		  
 */
void init_terminals()
{
	clear_terminal();
	int id;
	/* loop through the 3 terminals and initialize each terminal to a known state */
	for(id = 0; id < NUM_TERMINALS; id++)
	{
		terminal[id].terminal_id = id;

		/* set starting position to top left */
		terminal[id].x_pos = START_X_POS;
		terminal[id].y_pos = START_Y_POS;

		clear_kb_buf(terminal[id].keyboard_buf);
		terminal[id].curr_key_index = 0;

		/* initialize to the new line character not having been pressed */
		terminal[id].enter_pressed = UNPRESSED;	
		terminal[id].is_running = NO;

		memcpy((void*) video_buf[id], (void*) VIDEO, PAGE_SIZE);
	}

	clear_term_video_memory(ALL_TERMINALS);

	active_terminal_id = START_TERMINAL;
}

/* 
 * clear_term_video_memory
 *   DESCRIPTION: Clears a video memory buffer for a given terminal - or all three.
 *   INPUTS: terminal_id - The terminal whose memory we want to clear. 3 - clears all three terminal's video memory buffer.
 *   OUTPUTS: Video memory is cleared.
 *   SIDE EFFECTS: none
 * 	 WARNING: none	
 *	 AUTHOR: Michael Bazzoli	  
 */
uint8_t clear_term_video_memory(uint8_t terminal_id)
{
	int i;

	if(terminal_id > NUM_TERMINALS)
		return -1;

	/* clear all terminal's video memory - i.e. terminal_id passed was 3 */
	if(terminal_id == NUM_TERMINALS)
	{
		/* same as lib.c's clear basically */
		for(i = 0; i < NUM_ROWS*NUM_COLS; i++)
		{
			*(uint8_t *)(video_buf[0] + (i << 1)) = ' ';
			*(uint8_t *)(video_buf[0] + (i << 1) + 1) = ATTRIB;
			
			*(uint8_t *)(video_buf[1] + (i << 1)) = ' ';
			*(uint8_t *)(video_buf[1] + (i << 1) + 1) = ATTRIB;
		
			*(uint8_t *)(video_buf[2] + (i << 1)) = ' ';
			*(uint8_t *)(video_buf[2] + (i << 1) + 1) = ATTRIB;
		}
	}

	/* clear only the given terminal's video memory */
	else
	{
		for(i = 0; i < NUM_ROWS*NUM_COLS; i++)
		{
			*(uint8_t *)(video_buf[terminal_id] + (i << 1)) = ' ';
			*(uint8_t *)(video_buf[terminal_id] + (i << 1) + 1) = ATTRIB;	
		}	
	}

	return 0;
}

/* 
 * clear_kb_buf
 *   DESCRIPTION: Clears the keyboard buffer.
 *   INPUTS: none
 *   OUTPUTS: Keyboard buffer is cleared by setting each entry to the EOL character '\0'. 
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Clears the keyboard buffer. 
 * 	 WARNING: none		  
 */
void clear_kb_buf(volatile uint8_t* kb_buf)
{
	int i;

	for(i = 0; i < MAX_BUF_SIZE; i++)
	{
		kb_buf[i] = NULL_KEY;		// clear by setting it to the EOL character '\0'
	}

	terminal[active_terminal_id].curr_key_index = 0;	// reset the curr_key_index to 0
	terminal[active_terminal_id].enter_pressed = UNPRESSED;
}


/* 
* terminal_open
*   DESCRIPTION: Initializes the terminal.
*   INPUTS:  	filename - filename
*   OUTPUTS: Terminal is cleared. Terminal is initialized to a known state. 
*   RETURN VALUE: none
*   SIDE EFFECTS: Clears video memory.
* 	WARNING: none		  
*/
int32_t terminal_open(const uint8_t* filename)
{
	init_terminals();
	return 0;
}

/* 
* terminal_read
*   DESCRIPTION: Reads data from the keyboard buffer (until the enter key is pressed) and loads it into buf.
*   INPUTS:  	fd - file descriptor
*           	buf - The buffer in which we will store the current keyboard buffer
* 				nbytes - The number of bytes to load into buf.
*   OUTPUTS: buf is loaded with nbytes of the keyboard buffer (for a maximum of MAX_CHARS characters in any case)
*   RETURN VALUE: none
*   SIDE EFFECTS: Clears the keyboard buffer. 
* 	WARNING: none		  
*/
int32_t terminal_read(int32_t fd, void* buf, int32_t nbytes)
{
	int byteIdx, bytes_read;
	pcb_t* cur_pcb;

	GET_CUR_PCB(cur_pcb);

	/* load buf with the keys in the keyboard buffer only if enter has been pressed */
	while(((terminal[cur_pcb->terminal_id].enter_pressed) == UNPRESSED) || cur_pcb->terminal_id != active_terminal_id)
		;	// wait for ENTER to be pressed

	/* cast the void buf to buffer */
	uint8_t* buffer = (uint8_t*) buf;

	/* loop until byteIdx doesn't surpass MAX_CHARS and nbytes - we can copy extry chars after enter since they will be \0 */
	for(byteIdx = 0; (byteIdx < nbytes) && (byteIdx <= MAX_CHARS); byteIdx++)
	{
		buffer[byteIdx] = terminal[active_terminal_id].keyboard_buf[byteIdx];	// copy into the buffer all characters until (including) that at last_key_index
		
		if(buffer[byteIdx] == ENTER_KEY)
			break;
	}

	bytes_read = byteIdx + 1;

	clear_kb_buf(terminal[active_terminal_id].keyboard_buf);

	/* the read system call always returns the number of bytes read - in this case that is just byteIdx */
	return bytes_read;

}

/* 
* terminal_write
*   DESCRIPTION: Writes nbytes of data from buf to the screen.
*   INPUTS:  	fd - file descriptor
*           	buf - The buffer from which we will read to write to the screen. 
* 				nbytes - The number of bytes to write to the screen from buf.
*   OUTPUTS: Screen video memory is written. String is displayed on screen.
*   RETURN VALUE: none
*   SIDE EFFECTS: Writes to screen video memory.
* 	WARNING: none		  
*/
int32_t terminal_write(int32_t fd, const void* buf, int32_t nbytes)
{
	pcb_t* cur_pcb;

	cli();

	GET_CUR_PCB(cur_pcb);

	int byteIdx;
	uint8_t* buffer;

	buffer = (uint8_t*)buf;

	byteIdx = terminal_puts(buffer, 1, cur_pcb->terminal_id);

	sti();
	return byteIdx;		// return the number of bytes written
}

/* 
* terminal_close
*   DESCRIPTION: Closes the terminal driver.
*   INPUTS:  	fd - file descriptor
*   OUTPUTS: Closes the terminal driver.
*   RETURN VALUE: none
*   SIDE EFFECTS: none
* 	WARNING: none		  
*/
int32_t terminal_close(int32_t fd)
{
	return 0;
}

/* 
* sleep
*   DESCRIPTION: imitation of the sleep function found in unistd.h - not very accurate but suffices the purpose.
*   INPUTS:  	s - The number of seconds to sleep for.
*   OUTPUTS: none
*   RETURN VALUE: none
*   SIDE EFFECTS: sleep for s seconds approximately
* 	WARNING: none		  
*/
void sleep(int s)
{
	uint32_t i;
	uint32_t one_s = ONE_SECOND_LOOP;	// it takes about 1 second to perform 99999999 loops
	for(i = 0; i < ((uint32_t)s)*one_s; i++);
}

/*
 * switch_terminal
 *   DESCRIPTION: switch to the three different terminal screen
 *   INPUTS:  	idx --- the terminal id
 *   OUTPUTS: output the new terminal screen
 *   RETURN VALUE: none
 *   SIDE EFFECTS: switch to a new terminal screen
 * 	WARNING: none
 */
int32_t switch_terminal(uint32_t idx)
{
	if(idx == active_terminal_id)
		return 0;	// do nothing
	if(idx >= NUM_TERMINALS)
		return -1;

	memcpy((void*) video_buf[active_terminal_id], (void*) VIDEO, PAGE_SIZE);
	video_map_4kb((uint32_t) video_buf[active_terminal_id], (uint32_t) USER_VIDEO + active_terminal_id * PAGE_SIZE, active_terminal_id);
	memcpy((void*) VIDEO, (void*) video_buf[idx], PAGE_SIZE);
	video_map_4kb((uint32_t) VIDEO, (uint32_t) USER_VIDEO + idx * PAGE_SIZE, idx);

	/* set the amount of chars in the keyboard handler to what the new terminal had */
	set_char_added(terminal[idx].curr_key_index);	

	active_terminal_id = idx;
    set_cursor(active_terminal_id);
    bar_on();
    
	
	return 0;
}
