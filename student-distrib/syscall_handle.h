/**
 * The interface to all 10 system call handlers
 */

#ifndef _SYSCALL_HANDLER_H
#define _SYSCALL_HANDLER_H

#define NUM_SYSCALL 10
#define SYSCALL_ENTRY 0x80

#define SYS_HALT    1
#define SYS_EXECUTE 2
#define SYS_READ    3
#define SYS_WRITE   4
#define SYS_OPEN    5
#define SYS_CLOSE   6
#define SYS_GETARGS 7
#define SYS_VIDMAP  8
#define SYS_SET_HANDLER  9
#define SYS_SIGRETURN  10

#define ERROR_STATUS 255
#define PDE_INDEX 	32
#define BUF_SIZE 	128

#define LARGE_PAGE_SIZE (0x400000)
#define USER_ADDR 0x08000000
#define PDE_OFF 	(0x800000 + 0x87)

#define STARTING_ADDR 0x08048000
#define LOAD_SIZE 	(0x400000 - 0x48000)

#define EXE_KEY 0x464c457f


#ifndef _ASM

#include "lib.h"
#include "process.h"
#include "rtc.h"
#include "filesystem.h"
#include "syscall.h"
#include "paging.h"
#include "terminal.h"

/** The File System operation jump table **/
typedef struct fs_op_table {
	int32_t (*read) (int32_t, void*, int32_t);
	int32_t (*write) (int32_t, const void*, int32_t);
	int32_t (*open) (const uint8_t*);
	int32_t (*close) (int32_t);
} fs_op_table_t;

typedef struct syscall_desc {
	uint32_t function;
	uint32_t number_args;
} syscall_desc_t;

syscall_desc_t call_table[NUM_SYSCALL];

#define SET_SYSCALL(func, index, num_args) 						\
do { 															\
	if (index <= NUM_SYSCALL && index > 0)						\
	{															\
		call_table[index - 1].function = (uint32_t)func;		\
		call_table[index - 1].number_args = (uint32_t)num_args;	\
	}															\
} while(0)

#define SAVE_ESP_EBP(cur_pcb)									\
do {															\
	asm volatile(												\
		/* save the current esp and ebp to the pcb */			\
		"movl	%%esp, %0\n\t"									\
		"movl 	%%ebp, %1\n\t"									\
		:"=r"((cur_pcb->esp)), "=r"(cur_pcb->ebp));				\
} while(0)

/* 
 * init_syscall
 *   DESCRIPTION: Initialize the system call
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success. -1 on failure
 * 	 WARNING: Should be called before regist any specific handler
 *   AUTHOR : Zonglin Li
 */
int init_syscall();

/* 
 * call_syscall
 *   DESCRIPTION: The entry point for calling the system call in the kernel space
 *   INPUTS: syscall_num - The system call number
 *			 parameters -- The pointer to the array of the parameters
 *						   In the order of param1, param2, param3...
 *   OUTPUTS: none
 *   RETURN VALUE: The return value of the specific handler
 * 	 WARNING: This function will not be registered to the 0x80 call gate. Should only be used in the
 *			  kernel mode tasks. Also, there is no checking for the validity of the parameter array
 *	 AUTHOR: Zonglin Li
 */
int32_t call_syscall(int syscall_num, int32_t* parameters);

/* 
 * halt
 *   DESCRIPTION: Halt the current process, and return to parent
 *   INPUTS: status --- The return value
 *   OUTPUTS: none
 *   RETURN VALUE: The real return value to the parent process
 *	 AUTHOR: Michael Bazzoli
 */
int32_t halt (uint8_t status);

/* 
 * execute
 *   DESCRIPTION: Execute a new user level program. The parent program will be paused
 *   INPUTS: command --- The command and parameters
 *   OUTPUTS: none
 *   RETURN VALUE: The return value of the child program. Or -1 on error
 * 	 WARNING: The memory management is designed specially for this MP, using the assumption 
 * 			  that no user level program will use more than 4 MB memory. All programs 
 *			  share a common page directory, several common page tables. When switching is 
 *			  performed, only one page directory entry will be replaced.
 *	 AUTHOR: Zonglin Li
 */
int32_t execute (const uint8_t* command);

/* 
 * read
 *   DESCRIPTION: The syscall handler for generic read function
 *   INPUTS: fd ----- The file descriptor
 *			 buf ---- The buffer to copy the readed result
 *			 nbytes - Number of bytes should be read
 *   OUTPUTS: none
 *   RETURN VALUE: The actually number of byte loaded into the buffer. Or -1 if error
 *	 AUTHOR: Zonglin Li
 */
int32_t read (int32_t fd, void* buf, int32_t nbytes);

/* 
 * write
 *   DESCRIPTION: The syscall handler for generic write function
 *   INPUTS: fd ----- The file descriptor
 *			 buf ---- The buffer to copy the readed result
 *			 nbytes - Number of bytes should be read
 *   OUTPUTS: none
 *   RETURN VALUE: The actually number of byte readed out. Or -1 if error
 *	 AUTHOR: Zonglin Li
 */
int32_t write (int32_t fd, const void* buf, int32_t nbytes);

/* 
 * open
 *   DESCRIPTION: The syscall handler for generic open function
 *   INPUTS: filename --- The name of the file. Could be the device file
 *   OUTPUTS: none
 *   RETURN VALUE: The file descriptor. Or -1 if error
 *	 AUTHOR: Zonglin Li
 */
int32_t open (const uint8_t* filename);

/* 
 * close
 *   DESCRIPTION: The syscall handler for generic close function
 *   INPUTS: fd ----- The file descriptor
 *   OUTPUTS: none
 *   RETURN VALUE: 0 if success; -1 if fail
 *	 AUTHOR: Zonglin Li
 */
int32_t close (int32_t fd);

/*
 * getargs
 *   DESCRIPTION: Reads the program’s command line arguments into a user-level buffer
 *   INPUTS: buf --- user level buffer
 *           nbytes --- number of bytes to copy
 *   OUTPUTS: none
 *   RETURN VALUE: 0 if success; -1 if fail
 *	 AUTHOR: Tianhao Chi
 */
int32_t getargs (uint8_t* buf, int32_t nbytes);

int32_t vidmap (uint8_t** screen_start);

int32_t set_handler (int32_t signum, void* handler_address);

int32_t sigreturn (void);

#endif /* _ASM */

#endif /* _SYSCALL_HANDLER_H */
