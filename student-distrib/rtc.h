/**
 * Initialize the RTC and provide a function to change the interrupt rate
 * doc about rtc:
 *		http://wiki.osdev.org/RTC
 * 		http://stanislavs.org/helppc/cmos_ram.html
 */

#ifndef _RTC_H
#define _RTC_H 

#include "lib.h"
#include "types.h"
#include "spinlock.h"
#include "i8259.h"

#include "process.h"

#include "x86_desc.h"

#define RTC_SELECT_PORT 0x70
#define RTC_IO_PORT 0x71
#define RTC_IDT_HANDLER 0x28
#define REG_C 0x8C
#define REG_B 0x8B

/**
 * Open function
 */
int32_t rtc_read (int32_t fd, void* buf, int32_t nbytes);

/**
 * Write function
 */
int32_t rtc_write (int32_t fd, const void* buf, int32_t nbytes);

/**
 * Open function
 */
int32_t rtc_open (const uint8_t* filename);

/**
 * Close function
 */
int32_t rtc_close (int32_t fd);

/* initialize the rtc */
void init_rtc();

/* test handler for debug */
void handle_rtc();

/* set the rate of interrupt */
int rtc_set_rate(int rate);

#endif /* _RTC_H */
