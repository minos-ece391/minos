/**
 * The code for the interaction with PIT(programmable interval timer)
 * See http://kernelx.weebly.com/programmable-interval-timer.html
 * Author: Zonglin Li
 */
#include "types.h"
#include "i8259.h"
#include "x86_desc.h"
#include "lib.h"

#define PIT_CHNL0   0x40    //PIT Channel 0's Data Register Port
#define PIT_CHNL2   0x42    //PIT Channel 2's Data Register Port
#define PIT_CMD     0x43    //PIT Chip's Command Register Port
#define PIT_IRQ     0       //PIT will be wired to IRQ 0 on PIC
#define PIT_IDT_HANDLER (PIT_IRQ + 0x20)
#define DIV_CST     1193180 //Constant will be used to calculate the divisor

/*
 * init_pit
 *   DESCRIPTION: initialize the programmable interval timer to set the certian idt entry to the handler
 *   INPUTS: herts --- the frequency
 *           func --- the function of schedule
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: set the schedule function to the global variable
 *      WARNING: none
 */
void init_pit(int hertz, void (*func) ());

/*
 * set_channel0_freq
 *   DESCRIPTION: set the frequency of channel 0
 *   INPUTS: herts --- the frequency to set
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: output certian value to certian port
 *      WARNING: none
 */
void set_channel0_freq(int hertz);

/*
 * set_channel2_freq
 *   DESCRIPTION: set the frequency of channel2
 *   INPUTS: herts --- the frequency to set
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: output certian value to certian port
 *      WARNING: none
 */
void set_channel2_freq(int hertz);

/*
 * handle_pit
 *   DESCRIPTION: handler for PIT to handle the schedule function
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: handle the function of schedule() in process.c
 *      WARNING: none
 */
void handle_pit();
