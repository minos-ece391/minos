/**
 * The process decriptor struct
 * The global static array
 */ 

#ifndef _PROCSS_H
#define _PROCSS_H

#define STDIN  0					// The fd for stdin and stdout
#define STDOUT 1

#define INI_PCB (0x800000 - 0x2000)	// The initial address of PCB

#define NUM_PROCESS 6				// Max num of processes
#define STACK_SIZE (0x2000)			// size of the kernel stack

#define NUM_SIGNAL 8

#define NOT_RUNNING 0
#define RUNNING 1
#define SLEEPING 2

#include "types.h"
#include "syscall_handle.h"
#include "terminal.h"
#include "rtc.h"
#include "pit.h"
#include "lib.h"

#if (NUM_PROCESS) < (NUM_TERMINALS)
#error "Max number of terminal larger than max number of process" 
#endif

#define GET_CUR_PCB(variable) 				\
do { 										\
	asm volatile (							\
		"pushl 	%%ecx\n\t"					\
		"movl 	$0xffffe000,%%ecx\n\t"		\
		"andl 	%%esp,%%ecx\n\t"			\
		"movl 	%%ecx, %0\n\t"				\
		"popl	%%ecx\n\t"					\
		: "=r"(variable));					\
} while(0)

#define GET_PCB(pid)						\
		((INI_PCB) - pid * (STACK_SIZE)) 	

/** The signal action **/
typedef struct sig_action {
	uint32_t handler;
	uint32_t flags;						// The least significant bit is whether pending, the bit 
										// right next to will be whether masked out
} sig_action_t __attribute__((aligned(NUM_SIGNAL)));

/** The signal descriptor **/
typedef struct sig_desc {
	sig_action_t action_list[NUM_SIGNAL] __attribute__((aligned(8 * NUM_SIGNAL)));
	uint32_t count;
	uint32_t mask_all;
} sig_desc_t __attribute__((aligned(16 * (NUM_SIGNAL))));

/** The file descriptor of in the PCB **/
typedef struct file_desc {
	uint32_t file_op_table_ptr;
	uint32_t inode;
	uint32_t file_position;
	uint32_t flags;
} file_desc_t __attribute__((aligned(16)));

/** The PCB struct **/
typedef struct pcb {
	file_desc_t file_desc_array[8] __attribute__((aligned(128)));
	sig_desc_t sig_desc;
	struct pcb* parent;
	uint32_t esp0;
	uint32_t esp;
	uint32_t ebp;
	uint32_t eip;				// should be saved when switching out. And will be loaded when switching into
	uint8_t pid;				// Process ID
	uint8_t terminal_id;		// The terminal id (inherented from parent)
	uint16_t running_status;  	// 0: not running; 1: running; 2: sleeping
	uint8_t  args[128];
	uint32_t pde;				// the pde for the user program address space
	uint16_t timer;				// the timer for sleeping
	int16_t  rtc_freq; 			// The frequency of rtc for this process, if used. If not used
								// this should be -1;
} pcb_t __attribute__((aligned(STACK_SIZE)));

int current_process;

int avail_process[NUM_PROCESS + 1];

/* 
 * is_last_process
 *   DESCRIPTION: Checks if there is only 1 active process
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: True - There is only 1 process active; False - There are 2 or more active processes.
 * 	 WARNING: 
 *	 AUTHOR: Michael Bazzoli
 */
int is_last_process();

/* 
 * init_pcb
 *   DESCRIPTION: Initialize the pcb and kernel stack for a process
 *   INPUTS: parent -- The parent pcb struct pointer
 *   OUTPUTS: none
 *   RETURN VALUE: The actually number of byte loaded into the buffer. Or -1 if no more space for process
 *	 AUTHOR: Zonglin Li
 */
int init_pcb (pcb_t* parent);

/* 
 * free_pcb
 *   DESCRIPTION: Free up the pcb of a curtain process
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *	 AUTHOR: Zonglin Li
 */
void free_pcb(int pid);

/* 
 * init_base_shells
 *   DESCRIPTION: Initialize all the required base shells.
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *	 AUTHOR: Zonglin Li
 */
void init_base_shells();

/* 
 * init_base_shell
 *   DESCRIPTION: Initialize one of the base shell. The shell will not
 *	 			  execute right away, but will wait for the scheduler to 
 *				  activate it
 *   INPUTS: terminal_id --- which terminal to bind this shell to
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success,-1 on error
 *	 AUTHOR: Zonglin Li
 */
int init_base_shell(int terminal_id);

/* 
 * init_schedule
 *   DESCRIPTION: Initialize the scheduler
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *	 AUTHOR: Zonglin Li
 */
void init_schedule();

/* 
 * schedule
 *   DESCRIPTION: Schedule to the next process. 
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *	 AUTHOR: Zonglin Li
 */
void schedule();

#endif /* _PROCSS_H */ 
