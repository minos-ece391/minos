#include "lib.h"
#include "x86_desc.h"
#include "exception.h"

/*
 * exception_init
 *   DESCRIPTION: Initialize the all the exceptions
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 * 	 WARNING: 	   Should be called during initialization
 */
void
exception_init(){
    int i;
    for (i = 0; i < 0x20; i++) {
        if (i == 0) {
            SET_IDT_ENTRY(idt[i], exception0);
        }
        if (i == 1) {
            SET_IDT_ENTRY(idt[i], exception1);
        }
        if (i == 2) {
            SET_IDT_ENTRY(idt[i], exception2);
        }
        if (i == 3) {
            SET_IDT_ENTRY(idt[i], exception3);
        }
        if (i == 4) {
            SET_IDT_ENTRY(idt[i], exception4);
        }
        if (i == 5) {
            SET_IDT_ENTRY(idt[i], exception5);
        }
        if (i == 6) {
            SET_IDT_ENTRY(idt[i], exception6);
        }
        if (i == 7) {
            SET_IDT_ENTRY(idt[i], exception7);
        }
        if (i == 8) {
            SET_IDT_ENTRY(idt[i], exception8);
        }
        if (i == 9) {
            SET_IDT_ENTRY(idt[i], exception9);
        }
        if (i == 10) {
            SET_IDT_ENTRY(idt[i], exception10);
        }
        if (i == 11) {
            SET_IDT_ENTRY(idt[i], exception11);
        }
        if (i == 12) {
            SET_IDT_ENTRY(idt[i], exception12);
        }
        if (i == 13) {
            SET_IDT_ENTRY(idt[i], exception13);
        }
        if (i == 16) {
            SET_IDT_ENTRY(idt[i], exception16);
        }
        if (i == 17) {
            SET_IDT_ENTRY(idt[i], exception17);
        }
        if (i == 18) {
            SET_IDT_ENTRY(idt[i], exception18);
        }
        if (i == 19) {
            SET_IDT_ENTRY(idt[i], exception19);
        }

    }
}
/*
 * exception0 ~ exceotion19
 *   DESCRIPTION: each exception printed out
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: print the exception out on the console
 */

int exception0(){
    printf("EXCEPTION: Divide Error\n");
    return 0;
}
int exception1(){
    printf("EXCEPTION: RESERVED\n");
    return 0;
}
int exception2(){
    printf("EXCEPTION: NMI Interrupt\n");
    return 0;
}
int exception3(){
    printf("EXCEPTION: Breakpoint\n");
    return 0;
}
int exception4(){
    printf("EXCEPTION: Overflow\n");
    return 0;
}
int exception5(){
    printf("EXCEPTION: BOUND Range Exceeded\n");
    return 0;
}
int exception6(){
    printf("EXCEPTION: Invalid Opcode\n");
    return 0;
}
int exception7(){
    printf("EXCEPTION: Device Not Available\n");
    return 0;
}
int exception8(){
    printf("EXCEPTION: Double Fault\n");
    return 0;
}
int exception9(){
    printf("EXCEPTION: Coprocessor Segment Overrun\n");
    return 0;
}
int exception10(){
    printf("EXCEPTION: Invalid TSS\n");
    return 0;
}
int exception11(){
    printf("EXCEPTION: Segment Not Present\n");
    return 0;
}
int exception12(){
    printf("EXCEPTION: Stack-Segment Fault\n");
    return 0;
}
int exception13(){
    printf("EXCEPTION: General Protection\n");
    return 0;
}
int exception16(){
    printf("EXCEPTION: x87 FPU Floating-Point Error\n");
    return 0;
}
int exception17(){
    printf("EXCEPTION: Alignment Check\n");
    return 0;
}
int exception18(){
    printf("EXCEPTION: Machine Check");
    return 0;
}
int exception19(){
    printf("EXCEPTION: SIMD Floating-Point Exception\n");
    return 0;
}
