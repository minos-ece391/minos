#include "x86_desc.h"
#include "idt_init.h"
#include "lib.h"

/* setup_idt - Xavier C.
 * DESCRIPTION: Setup for the Interrupt Descriptor Table (IDT)
 * INPUTS: none
 * OUTPUTS: Sets up the IDT
 * RETURN VALUE: none
 * SIDE EFFECTS: none
 * Note: this function is called in kernel.c
 */ 
void setup_idt(){
    int i;
    for (i = 0; i < 0xff; i++) {
        idt[i].seg_selector = KERNEL_CS;
        //exception
        if (i <= 0x1f) {
            idt[i].reserved4 = 0;
            idt[i].reserved3 = 0;
            idt[i].reserved2 = 1;
            idt[i].reserved1 = 1;
            idt[i].size = 1;
            idt[i].reserved0 = 0;
            idt[i].dpl = 0;
            idt[i].present = 1;
        }
        //interrupt
        if (i >= 0x20 && i <= 0x2f) {
            idt[i].reserved4 = 0;
            idt[i].reserved3 = 0;
            idt[i].reserved2 = 1;
            idt[i].reserved1 = 1;
            idt[i].size = 1;
            idt[i].reserved0 = 0;
            idt[i].dpl = 0;
            idt[i].present = 1;
        }
        //system call
        if (i == 0x80) {
            idt[i].reserved4 = 0;
            idt[i].reserved3 = 1;           // for trap gate
            idt[i].reserved2 = 1;
            idt[i].reserved1 = 1;
            idt[i].size = 1;
            idt[i].reserved0 = 0;
            idt[i].dpl = 3;
            idt[i].present = 1;
        }
    }
    return;
}
