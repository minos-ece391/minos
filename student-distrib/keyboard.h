#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#include "lib.h"
#include "spinlock.h"
#include "i8259.h"
#include "x86_desc.h"
#include "syscall_handle.h"
 
#define KEYBOARD_IR 	1
#define KEYBOARD_PORT 	0x60
#define NULL_SCAN_CODE 	0
#define KEYB_IDT_ENTRY  0x21
#define NULL_KEY        '\0'
#define ENTER_KEY		'\n'
#define CARRIAGE_RETURN_KEY '\r'

#define KEY_MODIFIER    4		// there are 4 possible modes we will support (i.e. all combinations of caps lock ON/OFF, SHIFT ON/OFF)
#define NUM_KEYS	   60

#define UNPRESSED       0
#define PRESSED         1


#define INT_START asm volatile("pusha");
#define INT_END asm volatile("popa"); \
	asm volatile("leave");	\
	asm volatile("iret");


#define ZERO_PRESSED 		0x0B
#define ONE_PRESSED 		0x2
#define	NINE_PRESSED 		0xA

#define ENTER_PRESSED 		0x1C
#define SPACE_PRESSED       0x39
#define BACKSPACE_PRESSED	0x0E
#define CAPS_PRESSED		0x3A		/* NOTE - Scan codes > CAPS_PRESSED should be ignored in CP2 (except RH_CONTROL_PRESSED : 0x61) */

#define SHIFT_LH_PRESSED	0X2A
#define SHIFT_LH_UNPRESSED	0XAA

#define SHIFT_RH_PRESSED	0x36
#define SHIFT_RH_UNPRESSED	0xB6

#define CTRL_PRESSED		0X1D
#define CTRL_UNPRESSED		0x9D

/* not used in CP2 */
#define ALT_PRESSED			0x38
#define ALT_UNPRESSED		0xB8

#define TAB_PRESSED			0x0F

#define F1_PRESSED          0x3B
#define F1_UNPRESSED        0xBB
#define F2_PRESSED          0x3C
#define F2_UNPRESSED        0xBC
#define F3_PRESSED          0x3D
#define F3_UNPRESSED        0xBD

#define F10_PRESSED 		0x44
#define F10_UNPRESSED		0xC4

#define ESC_PRESSED			0x01
#define ESC_UNPRESSED		0x81



#define TERMINAL_ZERO		0x00
#define TERMINAL_ONE		0x01
#define TERMINAL_TWO		0x02


/* initialize the keyboard */
extern void keyboard_init();

/* handler functions for the various key presses */
extern void handle_interrupt();
extern void handle_enter();
extern void handle_backspace();
extern void handle_key(uint8_t scan_code);
extern void set_char_added(uint8_t num_chars);


#endif /* _KEYBOARD_H */ 

