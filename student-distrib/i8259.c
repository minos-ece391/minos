/* i8259.c - Functions to interact with the 8259 interrupt controller
 * vim:ts=4 noexpandtab
 */

#include "i8259.h"
#include "lib.h"
#include "spinlock.h"

/* Interrupt masks to determine which interrupts
 * are enabled and disabled */
uint8_t master_mask; /* IRQs 0-7 */
uint8_t slave_mask; /* IRQs 8-15 */

/* The lock to prevent concurrent access to PIC*/
spinlock_t pic_lock = SPIN_LOCK_UNLOCK;

/* Initialize the 8259 PIC */
void
i8259_init(void)
{
	int i = 0;								 // loop counter
	uint32_t flags;							 // flags
	// spinlock_lock_save_irq(&pic_lock, flags);// lock and disable the interrupt
	cli_and_save(flags);
	master_mask = 0xFF;						 // initialize the master mask
	slave_mask  = 0xFF;					     // initialize the slave mask
	outb(master_mask, MASTER_8259_DAT_PORT); // mask all IMR on master
	outb(slave_mask, SLAVE_8259_DAT_PORT);	 // mask all IMR on slave
	outb(ICW1, MASTER_8259_CMD_PORT);		 // send ICW1 to master
	outb(ICW2_MASTER, MASTER_8259_DAT_PORT); // send ICW2 to master
	outb(ICW3_MASTER, MASTER_8259_DAT_PORT); // send ICW3 to master
	outb(ICW4, MASTER_8259_DAT_PORT);		 // send ICW4 to master
	outb(ICW1, SLAVE_8259_CMD_PORT);		 // send ICW1 to slave
	outb(ICW2_SLAVE, SLAVE_8259_DAT_PORT);	 // send ICW2 to slave
	outb(ICW3_SLAVE, SLAVE_8259_DAT_PORT);   // send ICW3 to slave
	outb(ICW4, SLAVE_8259_DAT_PORT);		 // send ICW4 to slave

	while (++i < SLEEP_LOOP_TIMES);			 // wait some time for PIC to finish initialize

	master_mask = 0xFF;						 // initialize the master mask
	slave_mask  = 0xFF;					     // initialize the slave mask
	outb(master_mask, MASTER_8259_DAT_PORT); // unmask all IMR on master
	outb(slave_mask, SLAVE_8259_DAT_PORT);	 // unmask all IMR on slave
	//printf("initialized i8259");			 // print for debug
	// spinlock_unlock_restore_irq(&pic_lock, flags); // unlock the restore the flags
	restore_flags(flags);
} 

/* 
 * enable_irq
 *   DESCRIPTION: Enable (unmask) the specified IRQ
 *   INPUTS: irq_num -- Which interrupt line to enable. This value is in Dec, and range is [0, 15];
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Disable the interrupt on this processor, and will also wait 
 * 				   sevel ms for PIC to finish
 * 	 WARNING: 	   No checking for the validity of the parameter. 
 */
void
enable_irq(uint32_t irq_num)
{
	uint32_t flags; 						// to store the flag
	uint8_t  mask;							// bit mask for slave and master
	// spinlock_lock_save_irq(&pic_lock, flags);// lock and disable the interrupt
	cli_and_save(flags);
	if (irq_num >= 8)						// slave PIC
	{
		mask = ~(1 << (irq_num - 8));
		slave_mask &= mask;
		outb(slave_mask, SLAVE_8259_DAT_PORT);	// write out the mask
		mask = ~(1 << CAS_IR);
		master_mask &= mask;
		outb(master_mask, MASTER_8259_DAT_PORT); // write out to the master	
		//printf("\nslave: %x\n", slave_mask);
		//printf("master: %x\n", master_mask);
	} else
	{
		mask = ~(1 << irq_num);
		master_mask &= mask;				
		outb(master_mask, MASTER_8259_DAT_PORT); // write out to the master	
		//printf("\nmaster: %x\n", master_mask);
	}
	// spinlock_unlock_restore_irq(&pic_lock, flags); // unlock the restore the flags
	restore_flags(flags);
}

/* 
 * disable_irq
 *   DESCRIPTION: Enable (unmask) the specified IRQ
 *   INPUTS: irq_num -- Which interrupt line to enable. This value is in Dec, and range is [0, 15];
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Disable the interrupt on this processor, and will also wait 
 * 				   sevel ms for PIC to finish
 * 	 WARNING: 	   No checking for the validity of the parameter. 
 */
void
disable_irq(uint32_t irq_num)
{
	uint32_t flags; 						// to store the flag
	uint8_t  mask;							// bit mask for slave and master
	// spinlock_lock_save_irq(&pic_lock, flags);// lock and disable the interrupt
	cli_and_save(flags);
	if (irq_num & 8)						// slave PIC
	{
		mask = (1 << (irq_num & 8));
		slave_mask |= mask;
		outb(slave_mask, SLAVE_8259_DAT_PORT);	// write out the mask
	} else
	{
		mask = (1 << irq_num);
		master_mask |= mask;				
		outb(master_mask, MASTER_8259_DAT_PORT); // write out to the master
	}
	// spinlock_unlock_restore_irq(&pic_lock, flags); // unlock the restore the flags
	restore_flags(flags);
}

/* 
 * send_eoi
 *   DESCRIPTION: Send end-of-interrupt signal for the specified IRQ
 *   INPUTS: irq_num -- Which interrupt line to EOI
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Read from the IMR on slave and master
 * 	 WARNING: 	   No checking for the validity of the parameter. 
 */
void
send_eoi(uint32_t irq_num)
{
	uint32_t flags;								// to store the flag
	// spinlock_lock_save_irq(&pic_lock, flags);	// Lock
	cli_and_save(flags);
	if (irq_num >= 8)							// slave
	{
		/* Send EOI to slave*/
		outb(EOI + (irq_num - 8), SLAVE_8259_CMD_PORT); 
		/* Send EOI to master*/
		outb(EOI + CAS_IR, MASTER_8259_CMD_PORT);
	} else
	{
		/* Send EOI to master*/
		outb(EOI + irq_num, MASTER_8259_CMD_PORT);
	}
	// spinlock_unlock_restore_irq(&pic_lock, flags); // unlock the restore the flags
	restore_flags(flags);
}


