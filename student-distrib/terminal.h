#ifndef _TERMINAL_H
#define _TERMINAL_H

#include "types.h"
#include "keyboard.h"

#define MAX_CHARS    127
#define MAX_BUF_SIZE 128
#define NUM_TERMINALS  3
#define START_X_POS    0
#define START_Y_POS    0
#define START_TERMINAL 0

#define SUCCESS 	   0
#define FAILURE		  -1

#define YES			   1
#define NO			   0

#define ONE_SECOND_LOOP 99999999

#define PAGE_SIZE (0x1000)
#define VIDEO_0 0xB9000
#define VIDEO_1 0xBA000
#define VIDEO_2 0xBB000
#define USER_VIDEO 0x10000000

#define	ALL_TERMINALS  NUM_TERMINALS

typedef struct {
	uint8_t terminal_id;							// 0, 1, 2: in CP5 we will support 3 terminals - this ID will be useful then

	/* Screen Location variables */
	/* x_pos and y_pos will be used to save the current state of the terminal by getting screen_x */
	volatile uint8_t x_pos;
	volatile uint8_t y_pos;

	volatile uint8_t keyboard_buf[MAX_BUF_SIZE];		// keyboard buffer which stores the characters to be displayed on a line
	volatile uint8_t curr_key_index;				// index of the first empty position (ranges from 0 to 127) - 127 indicates the buffer is full.
	volatile uint8_t enter_pressed;					// enter pressed flag
	volatile uint8_t is_running;
    volatile uint32_t color;

} terminal_info;

/* global variables */
extern terminal_info terminal[NUM_TERMINALS];	// each of the 3 entries corresponds to the terminal_info for a certain terminal
volatile uint8_t active_terminal_id;		// id (0,1,2) of active terminal - used for CP5ex

/* initialize the terminals to a known state */
void init_terminals();
/* clear the keyboard buffer and sets associated parameters accordingly */
void clear_kb_buf(volatile uint8_t* kb_buf);

uint8_t clear_term_video_memory(uint8_t terminal_id);


/* ORWC system calls for terminal driver */
int32_t terminal_open(const uint8_t* filename);
int32_t terminal_read(int32_t fd, void* buf, int32_t nbytes);
int32_t terminal_write(int32_t fd, const void* buf, int32_t nbytes);
int32_t terminal_close(int32_t fd);

/* Function to put intervals during test */
void sleep(int s);
int32_t switch_terminal(uint32_t idx);

#endif /* _TERMINAL_H */
