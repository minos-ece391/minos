/**
 * The RTC driver
 * Author -- Zonglin Li
 */

#include "rtc.h"

/* The lock to prevent concurrent access to RTC*/
// static spinlock_t rtc_lock = SPIN_LOCK_UNLOCK;
// keep track of the interrupt
static volatile int rtc_status = 0;

/* 
 * init_rtc
 *   DESCRIPTION: Initialize the rtc. Disable the NMI and assign IRQ 8 to RTC
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Disables the interrupt.
 * 	 WARNING: Be sure to install the interrupt handler before calling this function.
 *			  since the default rate which is 1024 HZ will be selected. 
 *   AUTHOR: Zonglin Li
 */
void init_rtc()
{ 
	uint32_t flags;
	uint8_t  valu_b;								// The value in the register B
	cli_and_save(flags);
	SET_IDT_ENTRY(idt[RTC_IDT_HANDLER], handle_rtc);
	outb(REG_B, RTC_SELECT_PORT);					// select register B and disable NMI (clear bit 7)
	valu_b = inb(RTC_IO_PORT);						// Read immediately to prevent unexpected things
	outb(REG_B, RTC_SELECT_PORT);					// write out again (a read will reset the index to register D)
	outb(valu_b | 0x40, RTC_IO_PORT);				// set bit 6 to enable the periodic interrupt
	rtc_set_rate(2);
	rtc_status = 0;
	enable_irq(IRQ_8);
	restore_flags(flags);
}

/* 
 * shut_down
 *   DESCRIPTION: Shut down the RTC
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Disables the interrupt.
 * 	 WARNING: Be sure to install the interrupt handler before calling this function.
 *			  since the default rate which is 1024 HZ will be selected. 
 *   AUTHOR: Zonglin Li
 */
void shut_down()
{
	uint32_t flags;
	uint8_t  valu_b;								// The value in the register B
	cli_and_save(flags);
	outb(REG_B, RTC_SELECT_PORT);					// select register B and disable NMI (clear bit 7)
	valu_b = inb(RTC_IO_PORT);						// Read immediately to prevent unexpected things
	outb(REG_B, RTC_SELECT_PORT);					// write out again (a read will reset the index to register D)
	outb(valu_b & 0x4BF, RTC_IO_PORT);				// clear bit 6 to disable the periodic interrupt
	disable_irq(IRQ_8);								// disable the irq line 8
	restore_flags(flags);
}

/* 
 * handle_rtc
 *   DESCRIPTION: Handle a RTC interrupt - Interrupt handler
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Sends EOI for IR line 8
 * 	 WARNING: 	 
 *   AUTHOR: Zonglin Li
 */
void handle_rtc()
{
	uint32_t flags;
	asm volatile("pusha");
	cli_and_save(flags);
	rtc_status = 1;
	// schedule();
	outb(REG_C, RTC_SELECT_PORT);	// select register C
	inb(RTC_IO_PORT);		// just throw away contents
	send_eoi(IRQ_8);	// send EOI for IR line 8
	restore_flags(flags);
	asm volatile("popa");
	asm volatile("leave");
	asm volatile("iret");
}

/* 
 * rtc_set_rate
 *   DESCRIPTION: Set the rate of periodic interrupt of PIC
 *   INPUTS: the number of interrupt per second. Should be the power of 2
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success, -1 on failure
 *   SIDE EFFECTS: 
 *   AUTHOR: Zonglin Li
 */
int rtc_set_rate(int rate)
{
	int i = 0;
	if (rate == 0x400 || rate == 0x200 || rate == 0x100 || rate == 0x80 || rate == 0x40 ||
		rate == 0x20  || rate == 0x10  || rate == 0x08  || rate == 0x04 || rate == 0x02)
	{
		while(rate < 0x10000)
		{
			rate <<= 1;
			i++;
		}
		i &= 0x0F;
		outb(0x8A, 0x70);		// set index to register A, disable NMI
		char prev=inb(0x71);	// get initial value of register A
		outb(0x8A, 0x70);		// reset index to A
		outb((prev & 0xF0) | i, 0x71); //write only our rate to A. Note, rate is the bottom 4 bits.
		return 0;
	}
	printf("invalid rate = %x\n", rate);
	return -1;
}

/* 
 * read
 *   DESCRIPTION: Wait for the next rtc interrput
 *   INPUTS: none useful so far
 *   OUTPUTS: none
 *   RETURN VALUE: 0 
 *   SIDE EFFECTS: Diasble the interrupt
 *   AUTHOR: Zonglin Li
 */
int32_t rtc_read (int32_t fd, void* buf, int32_t nbytes)
{
	uint32_t flags;
	pcb_t* cur_pcb;
	GET_CUR_PCB(cur_pcb);
	if (cur_pcb->rtc_freq == -1)
		while(1);					// haven't opened yet. No "interrupt", no return 
	while (1)
	{
		if (rtc_status == 1 )
		{
			cli_and_save(flags);
			rtc_status = 0;
			restore_flags(flags);
			break;
		}
	}
	return 0;
}

/* 
 * write
 *   DESCRIPTION: Set the frequency of the RTC
 *   INPUTS: fd ----- file descriptor
 *			 buf ---- The buffer contains the thing to write
 * 			 nbytes - The number of bytes will be written out
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success 
 *   SIDE EFFECTS:
 *	 WARNING: Only first four bytes will be accepted
 *   AUTHOR: Zonglin Li
 */
int32_t rtc_write (int32_t fd, const void* buf, int32_t nbytes)
{
	const uint32_t* buf_pt = buf;
	pcb_t* cur_pcb;
	GET_CUR_PCB(cur_pcb);
	uint32_t rate = buf_pt[0];
	if (rtc_set_rate(rate) == -1)
		return -1;
	cur_pcb->rtc_freq = rate;
	return 0;
}

/* 
 * open
 *   DESCRIPTION: Initialize the RTC
 *   INPUTS: No used parameter
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success 
 *   SIDE EFFECTS:
 *	 WARNING: Will not reinitialize the RTC. 
 *   AUTHOR: Zonglin Li
 */
int32_t rtc_open (const uint8_t* filename)
{
	uint32_t freq = 2;
	return rtc_write(0, &freq, 4);
}

/* 
 * close
 *   DESCRIPTION: Shut down the RTC
 *   INPUTS: No used parameter
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success 
 *   SIDE EFFECTS: Disable the IRQ line 8
 *	 WARNING: 
 *   AUTHOR: Zonglin Li
 */
int32_t rtc_close (int32_t fd)
{
	// shut_down();
	// printf("rtc closed\n");
	pcb_t* cur_pcb;
	GET_CUR_PCB(cur_pcb);
	cur_pcb->rtc_freq = -1;
	return 0;
}
