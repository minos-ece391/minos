/**
 * The process decriptor struct
 * The global static array 
 */ 

#include "process.h"

static fs_op_table_t terminal_funcs = {&terminal_read, &terminal_write, &terminal_open, &terminal_close};

static int cur_pid;					// should be initialized in init_schedule
/* 
 * is_last_process
 *   DESCRIPTION: Checks if there is only 1 active process
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: True - There is only 1 process active; False - There are 2 or more active processes.
 * 	 WARNING: 
 *	 AUTHOR: Michael Bazzoli
 */
int is_last_process()
{
	int cnt = 0;
	int i;

	for(i = 0; i < NUM_PROCESS; i++)
	{
		if(avail_process[i] == 1)
		{	
			cnt++;
		}
	}

	if(cnt > 1)
		return 0;	// there is more than 1 precess active
	else
		return 1;	// there is only 1 active process;
}

/* 
 * init_pcb
 *   DESCRIPTION: Initialize the pcb and kernel stack for a process
 *   INPUTS: parent -- The parent pcb struct pointer (if NULL, then will be treated as base shell)
 *   OUTPUTS: none
 *   RETURN VALUE: The actually number of byte loaded into the buffer. Or -1 if no more space for process
 *				   or on invalid parameter
 *	 AUTHOR: Zonglin Li
 */
int init_pcb (pcb_t* parent)
{
	int i, pid;
	uint32_t flags;
	cli_and_save(flags);
	pcb_t* cur_pcb;							// points to the start of the pcb (not global)
	for (pid = 0; pid <= NUM_PROCESS; pid++)
	{
		if (avail_process[pid] == 0)		// empty
		{
			avail_process[pid] = 1;
			break;
		}
	}
	if (pid == NUM_PROCESS + 1)					// no space
		return -1;
	cur_pcb = (pcb_t*)GET_PCB(pid);
	cur_pcb->parent = parent;				// Set the parent (can be NULL) of this process
	cur_pcb->pid = pid;						// set the current pid
	cur_pcb->esp = cur_pcb->esp0 = cur_pcb->ebp = (uint32_t)cur_pcb + STACK_SIZE - 4;	// initialize both values
	cur_pcb->running_status = 0;
	cur_pcb->terminal_id = 0;
	cur_pcb->pde = PDE_OFF + pid * LARGE_PAGE_SIZE;
	for (i = 0; i < 8; i++)
	{
		cur_pcb->file_desc_array[i].flags = 0;	// No file current opened
	}
	/* Clear the signal descriptor */
	// uint8_t* sig_desc_addr = (uint8_t*)cur_pcb->sig_desc;
	// for (i = 0; i < 8 * (NUM_SIGNAL + 1); i++)
	// {
	// 	sig_desc_addr[i] = 0;
	// }
	cur_pcb->rtc_freq = -1;				// No rtc opened yet
	cur_pcb->file_desc_array[STDIN].file_op_table_ptr = (uint32_t)&terminal_funcs;		// stdin 
	cur_pcb->file_desc_array[STDIN].flags = 1;								// set as in use
	cur_pcb->file_desc_array[STDOUT].file_op_table_ptr = (uint32_t)&terminal_funcs;	// stdout 
	cur_pcb->file_desc_array[STDOUT].flags = 1;								// set as in use
	restore_flags(flags);
	return pid;
}

void free_pcb(int pid)
{
	int i;
	for (i = 2; i < 8; i++)
	{
		close(i);
	}
	avail_process[pid] = 0;
}

void init_schedule()
{
	init_pit(64, &schedule);
}

void schedule()
{
	pcb_t* next_pcb, * cur_pcb;
	int counter = NUM_PROCESS;
	while (counter-- > 0)
	{
		cur_pid = (cur_pid + 1) % (NUM_PROCESS);
		if (avail_process[cur_pid] != 0)
		{
			next_pcb = (pcb_t*)GET_PCB(cur_pid);
			if (next_pcb->running_status == 1)			// running
				break;
			if (next_pcb->running_status == 2)			// sleeping
			{
				if (--(next_pcb->timer) == 0)			// wake up
					break;
			}
		}
	}
	if (counter < 0)
		return;
	cli();
	GET_CUR_PCB(cur_pcb);
	/* get the context for next process */
	// restore the freq of the RTC
	if (next_pcb->rtc_freq != -1)
	{
		rtc_set_rate(next_pcb->rtc_freq);
	}
	/* save the current status */
	SAVE_ESP_EBP(cur_pcb);
	next_pcb->running_status = RUNNING;
	tss.esp0 = next_pcb->esp0;
	tss.ss0  = KERNEL_DS;
	asm volatile(
		"leal	back, %0\n\t"
		:"=r"(cur_pcb->eip));
	setpde(PDE_INDEX, next_pcb->pde);
	/* jump to next process*/
	asm volatile(
		"movl	%3, %%eax\n\t"
		"movl 	%0, %%ebp\n\t"
		"movl 	%1, %%esp\n\t"
		"jmp 	*%2\n"
		"back:\n\t"
		"sti\n\t"
		:													/* output */
		:"r"(next_pcb->ebp),"r"(next_pcb->esp), "r"(next_pcb->eip), "r"(next_pcb->ebp)
		:"eax");
}

int init_base_shell(int terminal_id)
{
	pcb_t* shell_pcb;
	uint32_t temp_pde = getpde(PDE_INDEX);
	uint32_t usr_eip, usr_cs, usr_ss;
	usr_cs = USER_CS;
	usr_ss = USER_DS;
	int pid = init_pcb(NULL);
	if (pid < 0)
		return -1;
	clear_terminal();
	shell_pcb = (pcb_t*) GET_PCB(pid);
	shell_pcb->running_status = SLEEPING;
	// shell_pcb->timer = terminal_id * 40;		// for testing purposes. Should be 1
	shell_pcb->timer = 1;
	shell_pcb->terminal_id = terminal_id;
	setpde(PDE_INDEX, shell_pcb->pde);
	int32_t fd = open((uint8_t*)"shell");
	if (fd < 0)
	{
		free_pcb(pid);								// free the pcb
		setpde(PDE_INDEX, temp_pde);				// remove teh paging
		return -1;
	}
	read(fd, (uint8_t*)STARTING_ADDR, LOAD_SIZE);	// read directly into the virtual address for the program
	close(fd);
	uint8_t* head = (uint8_t*)STARTING_ADDR;
	// if (head[0] != 0x7f || head[1] != 0x45 || head[2] != 0x4c || head[3] != 0x46)	// check the magic number
	if (((uint32_t*)head)[0] != EXE_KEY)
	{
		free_pcb(pid);								// free the pcb
		setpde(PDE_INDEX, temp_pde);				// remove teh paging
		return -1;
	}
	// usr_eip = (head[27] << 24) + (head[26] << 16) + (head[25] << 8) + head[24];
	usr_eip = ((uint32_t*)head)[6];
	setpde(PDE_INDEX, temp_pde);
	asm volatile(
		"leal 	next, %0"
		:"=r"(shell_pcb->eip));
	printf("initialized shell with terminal id = %d\n", shell_pcb->terminal_id);
	shell_pcb->esp -= 16;
	asm volatile(
		"movl	%0, (%3)\n\t"			// save usr cs onto stack
		"movl 	%1, -4(%3)\n\t"			// save usr ss onto stack
		"movl 	%2, -8(%3)\n\t"			// save usr eip onto stack
		:
		:"r"(usr_cs), "r"(usr_ss), "r"(usr_eip), "r"(shell_pcb->ebp)
		: "eax");
	asm volatile(
		"jmp finish"
		);
	asm volatile(
		/* artificially set up the stack */
		"next:\n\t"
		"movl	-4(%%eax), %%ecx\n\t"
		"movw	%%cx, %%ds\n\t"
		"pushl 	%%ecx\n\t"			// push the ss
		"pushl 	$0x83FFFFC\n\t"		// push the esp
		"pushfl \n\t"				// push the flags
		"popl	%%ecx\n\t"			// set the interrupt flag in the flags on stack
		"orl	$0x200, %%ecx\n\t"
		"pushl 	%%ecx\n\t"
		"pushl 	(%%eax)\n\t"		// push the cs
		"pushl 	-8(%%eax)\n\t"		// push the eip
		"iret\n"
		:
		:"r"(usr_cs), "r"(usr_ss), "r"(usr_eip)
		: "eax", "ecx");
	asm volatile(
		"finish:"
		);
	return 0;
}

void init_base_shells()
{
	int i = 1;
	for (; i < NUM_TERMINALS; i++)
		init_base_shell(i);
	current_process += 2;
}
