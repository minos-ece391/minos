/**
 * The generic system call handler. The handler itself will be registered at the 0x80 entry
 * in IDT
 */
#ifndef _SYSCALL_H
#define _SYSCALL_H

#ifndef _ASM

/* 
 * handle_syscall
 *   DESCRIPTION: Will be registered to the call gate 0x80 in IDT
 *   			  The parameters sould be passed through the registers
 *				  The usages for registers:
 *					 EAX: The system call number (actual call number see ../syscalls/ece391sysnum.h)
 *					 EBX: The first parameter
 *				   	 ECX: The second parameter
 *					 EDX: The third parameter
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: The return value of the specific handler
 *				   The value will be placed in the EAX register
 * 	 WARNING: This function will always return the control to the user-level task. 
 *			  So, use call_syscall instead is called in the kernel
 *   AUTHOR: Zonglin Li
 */
void handle_syscall();

#endif /* _ASM */

#endif /* _SYSCALL_H */
