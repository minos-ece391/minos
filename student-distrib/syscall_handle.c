/**
 * The interface to all 10 system call handlers
 */

#include "syscall_handle.h"
#include "lib.h"

fs_op_table_t jump_tables[3] = {
	{&rtc_read, &rtc_write, &rtc_open, &rtc_close},				// RTC : 0
	{&dir_read, &dir_write, &dir_open, &dir_close},				// DIR : 1
	{&fs_read, &fs_write, &fs_open, &fs_close}					// FIL : 2
};

/* 
 * init_syscall
 *   DESCRIPTION: Initialize the system call
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: 0 on success. -1 on failure
 * 	 WARNING: Should be called before regist any specific handler
 *   AUTHOR : Zonglin Li
 */
int init_syscall()
{
	memset(call_table, 0, sizeof(call_table));				// clear the syscall table
	SET_IDT_ENTRY(idt[SYSCALL_ENTRY], handle_syscall);		// set the call gate for syscall
	SET_SYSCALL(halt, SYS_HALT, 1);							// load the halt handler
	SET_SYSCALL(execute, SYS_EXECUTE, 1);					// load the execute handler
	SET_SYSCALL(read, SYS_READ, 3);							// load the read handler
	SET_SYSCALL(write, SYS_WRITE, 3);						// load the write handler
	SET_SYSCALL(open, SYS_OPEN, 1);							// load the open handler
	SET_SYSCALL(close, SYS_CLOSE, 1);						// load the close handler
	SET_SYSCALL(getargs, SYS_GETARGS, 2);					// load the getargs handler
	SET_SYSCALL(vidmap, SYS_VIDMAP, 1);						// load the vidmap handler
	// SET_SYSCALL(set_handler, SYS_SET_HANDLER, 2);			// load the sethandler handler
	// SET_SYSCALL(sigreturn, SYS_SIGRETURN, 0);				// load the sigreturn handler
	return 0;
}

/* 
 * call_syscall
 *   DESCRIPTION: The entry point for calling the system call in the kernel space
 *   INPUTS: syscall_num - The system call number
 *			 parameters -- The pointer to the array of the parameters
 *						   In the order of param1, param2, param3...
 *   OUTPUTS: none
 *   RETURN VALUE: The return value of the specific handler
 * 	 WARNING: This function will not be registered to the 0x80 call gate. Should only be used in the
 *			  kernel mode tasks. Also, there is no checking for the validity of the parameter array
 *	 AUTHOR: Zonglin Li
 */
int32_t call_syscall(int syscall_num, int32_t* parameters)
{
	int32_t return_val;					// The return value of the called function
	if (syscall_num - 1 <= NUM_SYSCALL)
	{
		asm volatile(
			"movl	%1, %%eax\n\t"
			"movl	0(%2), %%ebx\n\t"
			"movl	4(%2), %%ecx\n\t"
			"movl	8(%2), %%edx\n\t"
			"int	$0x80 \n\t"
			"movl	%%eax, %0 \n\t"
			: "=r"(return_val)
			: "r"(syscall_num), "r"(parameters)
			: "eax", "ebx", "ecx", "edx");
		return return_val;
	} else
	{
		return -1;
	}
}

int32_t halt (uint8_t status)
{
	uint8_t parent_pid;
	uint32_t parent_esp, parent_ebp;
	int32_t local_status = status;
	pcb_t * parent_pcb;
	pcb_t* cur_pcb;

	GET_CUR_PCB(cur_pcb);

	if (local_status == ERROR_STATUS)
	{
		local_status ++;
	}

	/* cli - we don't want to get interrupted during halt */
	cli();

	/* checks if the process we want to halt is the base shell */
	// if(is_last_process())
	// {
	// 	printf("Cannot halt the base shell!");
	// 	free_pcb(cur_pcb->pid);

	// }
	if (cur_pcb->parent == NULL || cur_pcb->parent == cur_pcb)	// one of the base shell
	{
		int terminal_id = cur_pcb->terminal_id;
		while (init_base_shell(terminal_id) < 0)
		{
			schedule();
		}
		free_pcb(cur_pcb->pid);
		schedule();
		return 256;
	}
	current_process --;
	parent_pcb = cur_pcb->parent;		// get parent pcb via the global active_pcb pointer
	parent_pid = parent_pcb->pid;
	parent_esp = parent_pcb->esp;
	parent_ebp = parent_pcb->ebp;

	free_pcb(cur_pcb->pid);

	/* restoring parent's paging - reset PDE at 128MB address - address holds page base of executable images */
	/* USER_PROG_0_BASE_ADDR + (parent_pid * PAGE_SIZE)) - User Program 0 is located at 8MB + (process ID * 4MB) */
	// set_pde_entry(EXEC_PDE_ADDR, USER_PROG_0_BASE_ADDR + (parent_pid * PAGE_SIZE));
	setpde(PDE_INDEX, parent_pcb->pde);								// set up the user space
	tss.esp0 = parent_pcb->esp0;
	tss.ss0  = KERNEL_DS;

	parent_pcb->running_status = 1;			// parent back to run

	// printf("end of halt, %d\n", local_status);

	asm volatile(
		"movl %0, %%ebp\n\t"
		"movl %1, %%esp\n\t"
		"movl %2, %%eax\n\t"
		"jmp finish_exe\n\t"
		:									/* output */
		:"r"(parent_ebp),"r"(parent_esp),"r"(local_status)	/* input */
		:"ebp","esp","eax");
	return 0;
}

/* 
 * execute
 *   DESCRIPTION: Execute a new user level program. The parent program will be paused
 *   INPUTS: command --- The command and parameters
 *   OUTPUTS: none
 *   RETURN VALUE: The return value of the child program. Or -1 on error
 * 	 WARNING: The memory management is designed specially for this MP, using the assumption 
 * 			  that no user level program will use more than 4 MB memory. All programs 
 *			  share a common page directory, several common page tables. When switching is 
 *			  performed, only one page directory entry will be replaced.
 *	 AUTHOR: Zonglin Li
 */
int32_t execute (const uint8_t* command)
{
	int pid;
	pcb_t* cur_pcb;
	pcb_t* new_pcb;
	uint32_t usr_cs = USER_CS;
	uint32_t usr_ss = USER_DS;
	uint32_t usr_eip;
	int i, j;
	char exe_name[BUF_SIZE];

	if (command == NULL)
		return -1;

	if (current_process == NUM_PROCESS)
		return -1;

	/** parse the command **/
	/** The args should be put to the in the pcb **/
	for (i = 0; i < strlen((char*)command); i++)
	{
		if (command[i] == '\0' || command[i] == ' ' || command[i] == '\n')
			break;
		exe_name[i] = (char)command[i];
	}
	GET_CUR_PCB(cur_pcb);			//macro: to get the pcb address
	exe_name[i] = '\0';
	
    if (exe_name[0] == 's' && exe_name[1] == 'h' && exe_name[2] == 'e' && exe_name[3] == 'l'
		&& exe_name[4] == 'l' && exe_name[5] == '\0')
	{
		clear_terminal();
	}
	cli();
	pid = init_pcb(cur_pcb);
	if (pid < 0)
	{
		sti();
		return -1;
	}
	new_pcb = (pcb_t*)GET_PCB(pid);
	i++;
	for (j = i; j < BUF_SIZE; j++)
	{
		new_pcb->args[j - i] = command[j];
		if (command[j] == '\0' || command[j] == ' ' || command[j] == '\n')
			break;
	}
	new_pcb->args[j - i] = '\0';
	new_pcb->terminal_id = cur_pcb->terminal_id;
	/** Set up the paging **/
	///// Here, for simplicity and because of one assumption 
	///// of the mp that no user level program will use more than
	///// 4 MB memory, we used same page directory and page table for all
	///// use level programs, just replace one page directory entry when switching to 
	///// a new process.
	setpde(PDE_INDEX, new_pcb->pde);				// set up the user space

	/** Load the exe file **/
	int32_t fd = open((uint8_t*)exe_name);
	if (fd < 0)
	{
		free_pcb(pid);								// free the pcb
		setpde(PDE_INDEX, cur_pcb->pde);			// remove teh paging
		sti();
		return -1;
	}
	read(fd, (uint8_t*)STARTING_ADDR, LOAD_SIZE);	// read directly into the virtual address for the program
	close(fd);
	uint8_t* head = (uint8_t*)STARTING_ADDR;
	// if (head[0] != 0x7f || head[1] != 0x45 || head[2] != 0x4c || head[3] != 0x46)	// check the magic number
	if (((uint32_t*)head)[0] != EXE_KEY)
	{
		setpde(PDE_INDEX, cur_pcb->pde);					// remove the paging
		free_pcb(pid);								// free the pcb
		sti();
		return -1;
	}
	// usr_eip = (head[27] << 24) + (head[26] << 16) + (head[25] << 8) + head[24];
	usr_eip = ((uint32_t*)head)[6];
	/** do the switching **/
	/* Update the esp0 in tss */
	tss.esp0 = new_pcb->esp0;	
	tss.ss0  = KERNEL_DS;
	SAVE_ESP_EBP(cur_pcb);
	cur_pcb->running_status = 0;	// parent stops running
	new_pcb->running_status = 1;	// child starts running

	current_process ++;
	asm volatile(
		/* artificially set up the stack */
		"movl	%1, %%eax\n\t"			// load the user stack segment descriptor
		"movw	%%ax, %%ds\n\t"
		"pushl 	%%eax\n\t"			// push the ss
		"pushl 	$0x83FFFFC\n\t"		// push the esp
		"pushfl \n\t"				// push the flags
		"popl	%%eax\n\t"			// set the interrupt flag in the flags on stack
		"orl	$0x200, %%eax\n\t"
		"pushl 	%%eax\n\t"
		"pushl 	%0\n\t"				// push the cs
		"pushl 	%2\n\t"				// push the eip
		"iret\n"
		"finish_exe:\n\t"			// label for halt
		"sti\n\t"					// sti
		"leave\n\t"
		"ret\n\t"
		:
		:"r"(usr_cs), "r"(usr_ss), "r"(usr_eip)
		: "eax");
	return 0;
}

/* 
 * read
 *   DESCRIPTION: The syscall handler for generic read function
 *   INPUTS: fd ----- The file descriptor
 *			 buf ---- The buffer to copy the readed result
 *			 nbytes - Number of bytes should be read
 *   OUTPUTS: none
 *   RETURN VALUE: The actually number of byte loaded into the buffer. Or -1 if error
 *	 AUTHOR: Zonglin Li
 */
int32_t read (int32_t fd, void* buf, int32_t nbytes)
{
	pcb_t* cur_pcb;					// points to the start of the initial(first) pcb
	GET_CUR_PCB(cur_pcb);			// get the current pcb
	if (fd != 1 && -1 < fd && fd < 8 && (fs_op_table_t*)cur_pcb->file_desc_array[fd].flags != 0)
	{
		fs_op_table_t* fs_table_ptr = (fs_op_table_t*)cur_pcb->file_desc_array[fd].file_op_table_ptr;
		return (*(fs_table_ptr->read))(fd, buf, nbytes);
	}
	return -1;
}

/* 
 * write
 *   DESCRIPTION: The syscall handler for generic write function
 *   INPUTS: fd ----- The file descriptor
 *			 buf ---- The buffer to copy the readed result
 *			 nbytes - Number of bytes should be read
 *   OUTPUTS: none
 *   RETURN VALUE: The actually number of byte readed out. Or -1 if error
 *	 AUTHOR: Zonglin Li
 */
int32_t write (int32_t fd, const void* buf, int32_t nbytes)
{
	pcb_t* cur_pcb;				// points to the initial (first) pcb
	GET_CUR_PCB(cur_pcb);		// get the current pcb from the %esp
	if (fd != 0 && -1 < fd && fd < 8 && (fs_op_table_t*)cur_pcb->file_desc_array[fd].flags != 0)
	{
		fs_op_table_t* fs_table_ptr = (fs_op_table_t*)cur_pcb->file_desc_array[fd].file_op_table_ptr;
		return (*(fs_table_ptr->write))(fd, buf, nbytes);
	}
	return -1;
}

/* 
 * open
 *   DESCRIPTION: The syscall handler for generic open function
 *   INPUTS: filename --- The name of the file. Could be the device file
 *   OUTPUTS: none
 *   RETURN VALUE: The file descriptor. Or -1 if error
 *	 AUTHOR: Zonglin Li
 */
int32_t open (const uint8_t* filename)
{
	pcb_t* cur_pcb;				// points to the initial (first) pcb
	GET_CUR_PCB(cur_pcb);		// get the current pcb from the %esp
	dentry_t temp;
	if (read_dentry_by_name(filename, &temp) == -1)
	{
		return -1;
	}
	int i = 0;
	for (; i < 8; i++)
	{
		if (cur_pcb->file_desc_array[i].flags == 0)		// not assigned yet
		{
			break;
		}
	}
	if (i >= 8)
	{
		return -1;
	}
	cur_pcb->file_desc_array[i].flags = 1;		// Mark as used
	cur_pcb->file_desc_array[i].inode = temp.inode_num;		// Set the inode
	cur_pcb->file_desc_array[i].file_position = 0; // Clean up the file position
	if (temp.file_type >= 0 && temp.file_type <= 2)
	{
		cur_pcb->file_desc_array[i].file_op_table_ptr = (uint32_t)&jump_tables[temp.file_type];
		fs_op_table_t* fs_table_ptr = (fs_op_table_t*)cur_pcb->file_desc_array[i].file_op_table_ptr;
		if((*(fs_table_ptr->open))(filename) == 0)
		{
			return i;
		}
	}
	cur_pcb->file_desc_array[i].flags = 0;		// restore as unused
	return -1;
}

/* 
 * close
 *   DESCRIPTION: The syscall handler for generic close function
 *   INPUTS: fd ----- The file descriptor
 *   OUTPUTS: none
 *   RETURN VALUE: 0 if success; -1 if fail
 *	 AUTHOR: Zonglin Li
 */
int32_t close (int32_t fd)
{
	pcb_t* cur_pcb;				// points to the initial (first) pcb
	GET_CUR_PCB(cur_pcb);		// get the current pcb from the %esp
	if (1 < fd && fd < 8 && cur_pcb->file_desc_array[fd].flags != 0)
	{
		// printf("close is called on fd = %d\n", fd);
		cur_pcb->file_desc_array[fd].flags = 0;
		fs_op_table_t* fs_table_ptr = (fs_op_table_t*)cur_pcb->file_desc_array[fd].file_op_table_ptr;
		return (*(fs_table_ptr->close))(fd);
	}
	return -1;
}

/*
 * getargs
 *   DESCRIPTION: Reads the program’s command line arguments into a user-level buffer
 *   INPUTS: buf --- user level buffer
 *           nbytes --- number of bytes to copy
 *   OUTPUTS: none
 *   RETURN VALUE: 0 if success; -1 if fail
 *	 AUTHOR: Tianhao Chi
 */
int32_t getargs (uint8_t* buf, int32_t nbytes)
{
    int i;
    pcb_t* cur_pcb;
    GET_CUR_PCB(cur_pcb);
    //error conditions
    if (buf == NULL) {
        return -1;
    }
    //copy to the user level buffer
    if (strlen((char*)cur_pcb->args) <= nbytes) {
        strcpy((int8_t*)buf, (char*)cur_pcb->args);
    }else{
        for (i = 0; i < nbytes; i++) {
            buf[i] = cur_pcb->args[i];
        }
    }
    
    return 0;
}

int32_t vidmap (uint8_t** screen_start)
{
	if((uint32_t) screen_start < USER_ADDR || (uint32_t) screen_start > USER_ADDR + LARGE_PAGE_SIZE)
		return -1;
	
	video_map_4kb((uint32_t) VIDEO, (uint32_t) USER_VIDEO + active_terminal_id * PAGE_SIZE, active_terminal_id);
	*screen_start = (uint8_t *)USER_VIDEO + active_terminal_id * PAGE_SIZE;

	return 0;
}

int32_t set_handler (int32_t signum, void* handler_address)
{
	return 0;
}

int32_t sigreturn (void)
{
	return 0;
}

