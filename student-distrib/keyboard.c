#include "keyboard.h"
#include "terminal.h"
#include "lib.h"
/* Keyboard global variables */

/* Shift/Caps modifier */
static int shift_pressed = UNPRESSED;
static int caps_lock_pressed = UNPRESSED;

/* CTRL flag */
static int ctrl_flag = UNPRESSED;
static int alt_flag = UNPRESSED;
static int char_added = 0;

/* Map of scancodes w/ respective modifiers. To access appropriate row use (2*caps_lock_pressed + shift_pressed). */
static uint8_t scancode_map[KEY_MODIFIER][NUM_KEYS] = {
	/* CAPS OFF - NO SHIFT */
	{'\0', '\0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '\0', '\0',
	 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\0', '\0', 'a', 's',
	 'd', 'f', 'g', 'h', 'j', 'k', 'l' , ';', '\'', '`', '\0', '\\', 'z', 'x', 'c', 'v', 
	 'b', 'n', 'm',',', '.', '/', '\0', '*', '\0', ' ', '\0'},
	/* CAPS OFF - SHIFT */
	{'\0', '\0', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '\0', '\0',
	 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '\0', '\0', 'A', 'S',
	 'D', 'F', 'G', 'H', 'J', 'K', 'L' , ':', '"', '~', '\0', '|', 'Z', 'X', 'C', 'V', 
	 'B', 'N', 'M', '<', '>', '?', '\0', '*', '\0', ' ', '\0'},
	/* CAPS ON - NO SHIFT */
	{'\0', '\0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '\0', '\0',
	 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\0', '\0', 'A', 'S',
	 'D', 'F', 'G', 'H', 'J', 'K', 'L' , ';', '\'', '`', '\0', '\\', 'Z', 'X', 'C', 'V', 
	 'B', 'N', 'M', ',', '.', '/', '\0', '*', '\0', ' ', '\0'},
	/* CAPS ON - SHIFT */
	{'\0', '\0', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '\0', '\0',
	 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '{', '}', '\0', '\0', 'a', 's',
	 'd', 'f', 'g', 'h', 'j', 'k', 'l' , ':', '"', '~', '\0', '\\', 'z', 'x', 'c', 'v', 
	 'b', 'n', 'm', '<', '>', '?', '\0', '*', '\0', ' ', '\0'}
};


/* SOME MACROS to handle the keyboard modifiers */
#define SHIFT_ON()			(shift_pressed = PRESSED);
#define SHIFT_OFF()			(shift_pressed = UNPRESSED);

/* XOR properly sets the caps_lock_pressed */
#define CAPS_TOGGLE()		(caps_lock_pressed ^= PRESSED);

#define CTRL_ON()			(ctrl_flag = PRESSED);
#define CTRL_OFF()			(ctrl_flag = UNPRESSED);
#define ALT_ON()            (alt_flag = PRESSED);
#define ALT_OFF()           (alt_flag = UNPRESSED);

/*  
 * keyboad_init
 *   DESCRIPTION: Initialize the keyboard
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Disables the interrupt
 * 	 WARNING: 	   Should be called during initialization
 */
void keyboard_init()
{
	//uint32_t flags;
	// spinlock_lock_save_irq(&kb_lock, flags);	// lock and disable the interrupt	
	SET_IDT_ENTRY(idt[KEYB_IDT_ENTRY], handle_interrupt);
	enable_irq(KEYBOARD_IR);					// enable the keyboard interrupt line
	// spinlock_unlock_restore_irq(&kb_lock, flags); // unlock the restore the flags
}

/* 
 * handle_interrupt
 *   DESCRIPTION: Keyboard interrupt handler
 *   INPUTS: none
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Disables the interrupt
 * 	 WARNING: 	   Should be called during initialization
 */
void handle_interrupt()
{
	uint8_t scan_code;
	uint32_t flags;
	INT_START
	cli_and_save(flags);
	// spinlock_lock_save_irq(&kb_lock, flags);	// lock and disable the interrupt
	scan_code = inb(KEYBOARD_PORT);				// read from keyboard port
    
	switch(scan_code)
	{
		case ENTER_PRESSED:
			//printf("ENTER_PRESSED\n");
			handle_enter();
			break;
        case ALT_PRESSED:
            ALT_ON();
            break;
        case ALT_UNPRESSED:
            ALT_OFF();
            break;
        case F1_PRESSED:
            if (alt_flag == PRESSED) {
                switch_terminal(TERMINAL_ZERO);
            }
            break;
        case F2_PRESSED:
            if (alt_flag == PRESSED) {
                switch_terminal(TERMINAL_ONE);
            }
            break;
        case F3_PRESSED:
            if (alt_flag == PRESSED) {
                switch_terminal(TERMINAL_TWO);
            }
            break;
		case BACKSPACE_PRESSED:
			handle_backspace();
			break;
		case SHIFT_RH_PRESSED:
		case SHIFT_LH_PRESSED:
			SHIFT_ON();
			break;
		case SHIFT_RH_UNPRESSED:
		case SHIFT_LH_UNPRESSED:
			SHIFT_OFF();
			break;
		case CTRL_PRESSED:
			CTRL_ON();
			break;
		case CTRL_UNPRESSED:
			CTRL_OFF();
			break;
		case CAPS_PRESSED:
			CAPS_TOGGLE();
			break;
		case TAB_PRESSED:
			break;
		default:
			handle_key(scan_code);
			break;
	}
	// spinlock_unlock_restore_irq(&kb_lock, flags); // unlock the restore the flags
	send_eoi(KEYBOARD_IR);
	restore_flags(flags);
	INT_END
}



/* 
 * handle_enter
 *   DESCRIPTION: Handles the enter key press.
 *   INPUTS: none
 *   OUTPUTS: terminal cursor goes to the new line. the enter key is added to the keyboard buffer and the enter_pressed flag is set.
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Video memory is changed as well as cursor position (see lib.c)
 * 	 WARNING: Returns from terminal_read if the function was called by user.
 */
void handle_enter()
{
	uint8_t kb_buf_idx;
	volatile uint8_t* kb_buf_ptr;

	terminal[active_terminal_id].enter_pressed = PRESSED;					// set the entry corresponding to the enter having been pressed = 1 for the active terminal window 

	/* add the enter key to the appropriate position in the keyboard buffer */
	kb_buf_idx = terminal[active_terminal_id].curr_key_index;				// get index where the enter key should be added
	kb_buf_ptr = terminal[active_terminal_id].keyboard_buf;
	kb_buf_ptr[kb_buf_idx] = ENTER_KEY;										// set the appropriate keyboard buffer entry to the enter key '\n'
	terminal[active_terminal_id].curr_key_index++;							// increment the curr_key_index		

	/* go to the next line - vertical scroll if necessary */
	new_line(active_terminal_id);
	char_added = 0;		// reset the number of chars added to the kb buffer to 0 

}

/* 
 * handle_backspace
 *   DESCRIPTION: Handles the backspace key press.
 *   INPUTS: none
 *   OUTPUTS: terminal cursor goes to the the previous characters position. The last echoed character is delete, if one char_added > 0
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Video memory is changed as well as cursor position (see lib.c)
 * 	 WARNING: 
 */
void handle_backspace()
{
	int idx;	// index at which to remove the current character in the buffer
	/* only delete if characters have been echoed */
	if(char_added > 0)
	{
		idx = --(terminal[active_terminal_id].curr_key_index);		/* decrement the current key index */
		terminal[active_terminal_id].keyboard_buf[idx] = NULL_KEY;
		//printf("%d",char_added);
		remc();
		char_added--;
	}
}

/* 
 * handle_key
 *   DESCRIPTION: Handles a key press.
 *   INPUTS: none
 *   OUTPUTS: Performs a certain function (such as clearing the terminal) if a sequence of presses was performed. Otherwise echoes a char, if valid.
 *   RETURN VALUE: none
 *   SIDE EFFECTS: Video memory is changed as well as cursor position (see lib.c)
 * 	 WARNING: 
 */
void handle_key(uint8_t scan_code)
{
	uint8_t c;
	int idx, mod;
	/* check if the character is valid */
	if(scan_code > NUM_KEYS)
		return;
	if (scan_code == ESC_PRESSED || scan_code == ESC_UNPRESSED || (scan_code >= F1_PRESSED && scan_code <= F10_PRESSED) || (scan_code >= F1_UNPRESSED && scan_code <= F10_UNPRESSED))
		return;

	/* compute the modifier */
	mod = 2*caps_lock_pressed + shift_pressed;
	/* get the character */
	c = scancode_map[mod][scan_code];		

    
	/* check if a clear_terminal() should be invoked */
	if(ctrl_flag == PRESSED)
	{
		switch(c)
		{
                //change the color of the text
            case '1':
                change_color(1);
                return;
            case '2':
                change_color(2);
                return;
            case '3':
                change_color(3);
                return;
            case '4':
                change_color(4);
                return;
            case '5':
                change_color(5);
                return;
                
			case 'c':
			case 'C':
				send_eoi(KEYBOARD_IR);
				printf("program terminated by keyboard\n");
				halt(0);
				return;
			case 'l':
			case 'L':
				clear_terminal();
				clear_kb_buf(terminal[active_terminal_id].keyboard_buf);
				char_added = 0;
				return;
		}
	}
	//test for mac user switch terminal
	else if(alt_flag == PRESSED)
	{
		switch(c)
		{
			case '1': 
				switch_terminal(TERMINAL_ZERO);
				return;
			case '2': 
				switch_terminal(TERMINAL_ONE);
				return;
			case '3': 
				switch_terminal(TERMINAL_TWO);
				return;

		}
	}
	else if(terminal[active_terminal_id].curr_key_index == MAX_CHARS)
		;// DO NOTHING - we will wait for an enter key
	/* echo the valid character */
	else
	{
		char_added++;
		idx = terminal[active_terminal_id].curr_key_index;		// get index where we will add the character c to in the keyboard buffer 
		terminal[active_terminal_id].keyboard_buf[idx] = c;	// add to the keyboard buffer the character represented by scan_code
		terminal[active_terminal_id].curr_key_index++;			// increment the first open position index in the kb buf
		terminal_putc(c, 0, active_terminal_id);				// add character to the terminal
	}

}

/* 
 * set_char_added
 *   DESCRIPTION: Sets the char_added global variable. Used when switching terminals.
 *   INPUTS: num_chars - the number of characters we wish to swt char_added to.
 *   OUTPUTS: Resets char_added.
 *   RETURN VALUE: none
 *   SIDE EFFECTS: none
 * 	 WARNING: 
 *	 AUTHOR: Michael Bazzoli	  
 */
void set_char_added(uint8_t num_chars)
{
	char_added = num_chars;
}
