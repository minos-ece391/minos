/**
 * things about spin_lock.
 * doc about spinlock:
 *		
 * Author: Zonglin Li 
 */
#ifndef _SPINLOCK_H
#define _SPINLOCK_H 

#define MULTIPROCESSOR 0					// change to 1 on mutiprocessor
#define SPIN_LOCK_UNLOCK { 0 }
#define SPIN_LOCK_LOCKED { 1 }

#include "lib.h"
#include "types.h"

typedef struct spinlock {
	volatile uint32_t lock;
} spinlock_t;

/* Lock the spin lock*/
void spinlock_lock(spinlock_t* lock);

/* Lock the spin lock and save the irq flags*/
void spinlock_lock_save_irq(spinlock_t* lock, uint32_t flags);

/* Unlock the spin lock*/
void spinlock_unlock(spinlock_t* lock);

/* Unlock and restore the flags*/
void spinlock_unlock_restore_irq(spinlock_t* lock, uint32_t flags);

#endif /* _SPINLOCK_H */
