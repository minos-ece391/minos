/* filesystem.h - Defines used in file system O/C/R/W
 * doc about i8259
 *		http://wiki.osdev.org/File_Systems
 * Author: Yuqi Wang
 */ 

#ifndef _FILESYSTEM_H
#define _FILESYSTEM_H

#include "types.h"
#include "process.h"
#include "lib.h"

/* MACROs */
#define MAX_NAME_LEN 32				/* maximum length of a file name */
#define MAX_DIR_LEN 63				/* maximum length of directory entries in the boot block */
#define MAX_DATA_BLOCK_LEN 1023 	/* maximum length of data blocks in a index node */
#define RES_LEN_24 24 				/* 24B reserved space in directory entry */
#define RES_LEN_52 52 				/* 52B reserved space in boot block */
#define BLOCK_SIZE 0x1000			/* size of each block : 4KB */

/* 64B dir. entry stucture */
typedef struct dentry_t {
	uint8_t file_name[MAX_NAME_LEN];
	uint32_t file_type;
	uint32_t inode_num;
	uint8_t reserved[RES_LEN_24];
} dentry_t;

/* boot block structure */
typedef struct boot_block_t {
	uint32_t dir_entry_num;
	uint32_t inodes_num;
	uint32_t data_blocks_num;
	uint8_t reserved[RES_LEN_52];
	dentry_t dir_entries[MAX_DIR_LEN];
} boot_block_t;

/* index nodes structure */
typedef struct inode_t {
	uint32_t length;
	uint32_t block_idx[MAX_DATA_BLOCK_LEN];
} inode_t;

/* initialize the fs */
void fs_init(boot_block_t* boot_ptr);

/* helper function */
int32_t read_dentry_by_name (const uint8_t * fname, dentry_t * dentry);
int32_t read_dentry_by_index (const uint32_t index, dentry_t * dentry);
int32_t read_data (uint32_t inode, uint32_t offset, uint8_t * buf, uint32_t length);

/* file system o/c/r/w */
int32_t fs_open(const uint8_t* filename);
int32_t fs_close(int32_t fd);
int32_t fs_read(int32_t fd, void * buf, int32_t nbytes);
int32_t fs_write(int32_t fd, const void* buf, int32_t nbytes);

/* file operations */
int32_t file_open(uint8_t * fname, dentry_t * buf);
int32_t file_close(int32_t fd);
int32_t file_read(uint32_t inode, uint32_t offset, uint8_t * buf, uint32_t length);
int32_t file_write(int32_t fd, const void* buf, int32_t nbytes);

/* directory operations */
int32_t dir_open(const uint8_t* filename);
int32_t dir_close(int32_t fd);
int32_t dir_read(int32_t fd, void * buf, int32_t nbytes);
int32_t dir_write(int32_t fd, const void* buf, int32_t nbytes);

#endif /* _FILESYSTEM_H */
