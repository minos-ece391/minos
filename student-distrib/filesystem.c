/* filesystem.c - Functions to interact with the file operations */

#include "filesystem.h"

boot_block_t * fs;				/* a global pointer to the start of the file system */

/* 
 * read_dentry_by_name 
 *   DESCRIPTION: Scan the file system to read the dir entry that corresponding to 
 *                the given file name
 *   INPUTS: fname -- file name
 * 			dentry -- buffer to store information about the dir entry, including 
 *						file name, file type, and inode#. 
 *   OUTPUTS: Information of the entry corresponding to the file name
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */

int32_t read_dentry_by_name (const uint8_t * fname, dentry_t * dentry)
{
	int i;	/* local variable */
	int fname_len;	/* length of input */
	int file_name_len; /* length of file name in directory entry */

	fname_len = strlen((int8_t *) fname);

	/* failure: non-existence input file name */
	if(fname_len == 0)
		return -1;

	/* limit the input file name to length allowed by the file system */
	if(fname_len >= MAX_NAME_LEN)
		fname_len = MAX_NAME_LEN - 1;

	for (i = 0; i < fs->dir_entry_num; i ++) 
	{
		file_name_len = strlen((int8_t *) fs->dir_entries[i].file_name); 
		// printf("debug: %s, i: %d, length: %d, type: %d\n", fs->dir_entries[i].file_name, i, file_name_len, fs->dir_entries[i].file_type);

		/* compare the length */
		if(fname_len == file_name_len) {
			/* compare the string */
			if (strncmp((int8_t *) fname, (int8_t *) fs->dir_entries[i].file_name, fname_len) == 0)
			{
				/* output info */
				strcpy((int8_t *) dentry->file_name, (int8_t *) fs->dir_entries[i].file_name);
				// printf("debug: %s, i: %d\n", fs->dir_entries[i].file_name, i);
				dentry->file_type = fs->dir_entries[i].file_type;
				dentry->inode_num = fs->dir_entries[i].inode_num;
				return 0;
			}
		}
	}
	/* failure: non-existance file name in directory entries */
	return -1;

}

/* 
 * read_dentry_by_index 
 *   DESCRIPTION: Scan the file system to read the dir entry that corresponding to 
 *                the given entry index
 *   INPUTS: index -- inode index
 * 			dentry -- buffer to store information about the dir entry, including 
 *						file name, file type, and inode#. 
 *   OUTPUTS: Information of the entry corresponding to the index
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */

int32_t read_dentry_by_index (const uint32_t index, dentry_t * dentry)
{
	/* failure: invalid index or non-existent file */
	if (index >= fs->dir_entry_num)
		return -1;

	/* failure: non-existence dir entry */
	if (strlen((int8_t *) fs->dir_entries[index].file_name) == 0)
		return -1;

	/* output info */
	strcpy((int8_t *) dentry->file_name, (int8_t *) fs->dir_entries[index].file_name);
	dentry->file_type = fs->dir_entries[index].file_type;
	dentry->inode_num = fs->dir_entries[index].inode_num;
	return 0;

}

/* 
 * read_data
 *   DESCRIPTION: 
 *   INPUTS: inode -- inode index
 * 			offset -- offset by bytes
 *			buf -- buffer pointer to store file info
 * 			length -- bytes copy into the buffer
 *   OUTPUTS: file data copied into the buffer
 *   RETURN VALUE: bytes length copied into the buffer
 *   SIDE EFFECTS: none
 */

int32_t read_data (uint32_t inode, uint32_t offset, uint8_t * buf, uint32_t length)
{
	inode_t * inode_addr; /* pointer to inode */
	uint32_t * data_base; /* pointer to the beginning of the data block */
	uint32_t inode_len, data_index, data_offset; /* calculated value */
	int32_t i; /* local variable */

	/* check the input parameter */
	if(inode >= fs->inodes_num || buf == NULL)
		// return -1;
		return 0;
	/* index node address */
	inode_addr = (inode_t *)((uint32_t) fs + (inode + 1) * BLOCK_SIZE);
	inode_len = inode_addr->length;

	/* failure: non-existence inode or offset */
	if(offset >= inode_len || inode_len == 0)
		// return -1;
		return 0;

	data_base = (uint32_t *)((uint32_t) fs + (fs->inodes_num + 1) * BLOCK_SIZE);

	i = 0;
	while (i < length)
	{
		if(offset + i >= inode_len)
			break;

		data_index = *(uint32_t *)((uint32_t) inode_addr + (1 + (i + offset)/BLOCK_SIZE) * 4);
		data_offset = (i + offset) % BLOCK_SIZE;
		*(uint8_t *)((uint32_t) buf + i) = *(uint8_t *)((uint32_t) data_base + data_index * BLOCK_SIZE + data_offset);
		// printf("base: %x; index: %d; offset: %d; data: %x\n ", data_base, data_index, data_offset, *(uint8_t *)((uint32_t) data_base + data_index * BLOCK_SIZE + data_offset));
		i ++;
	}
	if(i < length)
		buf[i] = '\0'; 

	if (i < length)
		buf[i] = '\0';

	return i;

}

/*
 * fs_init
 *   DESCRIPTION: initialize the file system set the global variable to the boot_ptr
 *   INPUTS: boot_ptr --- pointer of the module of the start of file system
 *   OUTPUTS: none
 *   RETURN VALUE: none
 *   SIDE EFFECTS: set the global variable fs
 */
void fs_init(boot_block_t* boot_ptr)
{
	fs = boot_ptr;
	printf("File system initialized at memory 0x%x\n", fs);
}

/* 
 * fs_open
 *   DESCRIPTION: setup the fs pointer to the beginning of the file system 
 *   INPUTS: filename -- pointer to the beginning memory block
 *   OUTPUTS: the memory addr of the file system
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */

int32_t fs_open(const uint8_t* filename)
{
	/* input parameter check */
	if (filename == NULL)
		return -1;

	// printf("File: %s opened\n", filename);
	return 0;
}

/* 
 * fs_close
 *   DESCRIPTION: close the current file system
 *   INPUTS: fd - not used
 *   OUTPUTS: indicate that the file system is closed. 
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */

int32_t fs_close(int32_t fd)
{
	// printf("File closed\n");
	return 0;
}

/* 
 * fs_read
 *   DESCRIPTION: read nbytes of the file corresponding to the file descriptor into * 				the buffer
 *   INPUTS: fd - file descriptor 
 * 			buf - pointer to the buffer to store file data
 * 			nbytes - the bytes of data we want to read into the buffer
 *   OUTPUTS: return the number of bytes copied into the buffer
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */

int32_t fs_read(int32_t fd, void * buf, int32_t nbytes)
{
	if(fs == NULL){
		printf("File system not initialized yet");
		return -1;
	}

	pcb_t* cur_pcb;				// points to the initial (first) pcb
	GET_CUR_PCB(cur_pcb);		// get the current pcb from the %esp
	uint32_t inode = cur_pcb->file_desc_array[fd].inode;
	uint32_t offset = cur_pcb->file_desc_array[fd].file_position;
	uint32_t bytes = read_data(inode, offset, (uint8_t *) buf, (uint32_t) nbytes);
	cur_pcb->file_desc_array[fd].file_position += bytes;

	return bytes;
}

/* 
 * fs_write
 *   DESCRIPTION: fulfill the structure, never used for a read only file system
 *   INPUTS: fd - file descriptor 
 * 			buf - pointer to the buffer to store file data
 * 			nbytes - the bytes of data we want to read into the buffer
 *   OUTPUTS: always -1 currently
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */


int32_t fs_write(int32_t fd, const void* buf, int32_t nbytes)
{
	printf("Read Only file system\n");
	return -1;
}

/* 
 * file_open
 *   DESCRIPTION: given a file name and buffer pointer, store the file info to the 
 *				buffer
 *   INPUTS: fname -- file name
 * 				buf - pointer to the buffer to store file info (file name, file 
 *				type, inode #)
 *   OUTPUTS: none
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */

int32_t file_open(uint8_t * fname, dentry_t * buf)
{
	return read_dentry_by_name(fname, buf);
}

/* 
 * file_close
 *   DESCRIPTION: close the current file
 *   INPUTS: fd - not used
 *   OUTPUTS: none
 *   RETURN VALUE: success: 0
 *   SIDE EFFECTS: none
 */

int32_t file_close(int32_t fd)
{
	return 0;
}


/* 
 * file_read
 *   DESCRIPTION: 
 *   INPUTS: inode -- inode index
 * 			offset -- offset by bytes
 *			buf -- buffer pointer to store file info
 * 			length -- bytes copy into the buffer
 *   OUTPUTS: file data copied into the buffer
 *   RETURN VALUE: bytes length copied into the buffer
 *   SIDE EFFECTS: none
 */

int32_t file_read(uint32_t inode, uint32_t offset, uint8_t * buf, uint32_t length)
{
	return read_data(inode, offset, buf, length);
}

/* 
 * file_write
 *   DESCRIPTION: fulfill the structure, never used for a read only file system
 *   INPUTS: fd - file descriptor 
 * 			buf - pointer to the buffer to store file data
 * 			nbytes - the bytes of data we want to read into the buffer
 *   OUTPUTS: always -1 currently
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */

int32_t file_write(int32_t fd, const void* buf, int32_t nbytes)
{
	return -1;
}

/* 
 * dir_open
 *   DESCRIPTION: given a file name and buffer pointer, store the file info to the 
 *				buffer
 *   INPUTS: fname -- file name
 * 				buf - pointer to the buffer to store file info (file name, file 
 *				type, inode #)
 *   OUTPUTS: none
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */

int32_t dir_open(const uint8_t* filename)
{
	if (filename == NULL)
		return -1;

	// printf("Directory: %s opened\n", filename);
	return 0;
}

/* 
 * dir_close
 *   DESCRIPTION: close the current file
 *   INPUTS: fd - not used
 *   OUTPUTS: none
 *   RETURN VALUE: success: 0
 *   SIDE EFFECTS: none
 */

int32_t dir_close(int32_t fd)
{
	// printf("directory closed\n");
	return 0;
}

/* 
 * dir_read
 *   DESCRIPTION: 
 *   INPUTS: offset -- offset by bytes
 *			buf -- buffer pointer to store file info
 * 			length -- bytes copy into the buffer
 *   OUTPUTS: file data copied into the buffer
 *   RETURN VALUE: bytes length copied into the buffer
 *   SIDE EFFECTS: none
 */

int32_t dir_read(int32_t fd, void * buf, int32_t nbytes)
{
	// int i = 0;	/* tracker */
	int file_name_len;
	uint8_t* char_buf = buf;

	memset(buf, 0, nbytes);

	pcb_t* cur_pcb;				// points to the initial (first) pcb
	GET_CUR_PCB(cur_pcb);		// get the current pcb from the %esp
	uint32_t index = cur_pcb->file_desc_array[fd].inode;
	uint32_t offset = cur_pcb->file_desc_array[fd].file_position;
	
	// while (index < fs->dir_entry_num)
	// {
	// 	file_name_len = strlen((int8_t *) fs->dir_entries[index].file_name); 
	// 	// printf("debug: %s, i: %d, length: %d, offset: %d, nbytes: %d\n", fs->dir_entries[index].file_name, i, file_name_len, offset, nbytes);

	// 	if(i + file_name_len - offset > nbytes) {
	// 		i --;
	// 		break;
	// 	}

	// 	strncpy((int8_t*) &char_buf[i], (int8_t*) &fs->dir_entries[index].file_name[offset], (uint32_t) file_name_len-offset);
	// 	i += file_name_len - offset;
	// 	offset = 0;
	// 	index++;
	// 	if(index < fs->dir_entry_num) {
	// 		char_buf[i] = '\n';
	// 		i ++;
	// 	}

	// }	

	if (offset < fs->dir_entry_num)
	{
		file_name_len = strlen((int8_t *) fs->dir_entries[index].file_name); 
		strncpy((int8_t*) char_buf, (int8_t*) &fs->dir_entries[index].file_name, (uint32_t) file_name_len);
		cur_pcb->file_desc_array[fd].file_position = offset + 1;
		cur_pcb->file_desc_array[fd].inode = offset + 1;
		return file_name_len;
	}

	return 0;
}

/* 
 * dir_write
 *   DESCRIPTION: fulfill the structure, never used for a read only file system
 *   INPUTS: fd - file descriptor 
 * 			buf - pointer to the buffer to store file data
 * 			nbytes - the bytes of data we want to read into the buffer
 *   OUTPUTS: always -1 currently
 *   RETURN VALUE: failure: -1
 *					success: 0
 *   SIDE EFFECTS: none
 */

int32_t dir_write(int32_t fd, const void* buf, int32_t nbytes)
{
	return -1;
}




