/* i8259.h - Defines used in interactions with the 8259 interrupt
 * controller
 * doc about i8259
 *		https://courses.engr.illinois.edu/ece391/references/8259A_PIC_Datasheet.pdf
 *		http://lxr.free-electrons.com/source/arch/x86/kernel/i8259.c
 * Author: Zonglin Li
 */ 

#ifndef _I8259_H
#define _I8259_H

#include "types.h"

/* Ports that each PIC sits on */
#define MASTER_8259_CMD_PORT 0x20
#define MASTER_8259_DAT_PORT 0x21
#define SLAVE_8259_CMD_PORT  0xA0
#define SLAVE_8259_DAT_PORT  0xA1

/* Casacade info*/
#define CAS_IR 2					// The line the slave is hooked to on master

/* Initialization control words to init each PIC.
 * See the Intel manuals for details on the meaning
 * of each word */
#define ICW1    0x11
#define ICW2_MASTER   0x20
#define ICW2_SLAVE    0x28
#define ICW3_MASTER   0x04
#define ICW3_SLAVE    0x02
#define ICW4          0x01

/* End-of-interrupt byte.  This gets OR'd with
 * the interrupt number and sent out to the PIC
 * to declare the interrupt finished */
#define EOI           0x60

/* Others */
#define SLEEP_LOOP_TIMES 100000

#define IRQ_8 8

/* Externally-visible functions */

/* Initialize both PICs */
void i8259_init(void);
/* Enable (unmask) the specified IRQ */
void enable_irq(uint32_t irq_num);
/* Disable (mask) the specified IRQ */
void disable_irq(uint32_t irq_num);
/* Send end-of-interrupt signal for the specified IRQ */
void send_eoi(uint32_t irq_num);

#endif /* _I8259_H */
