#ifndef PAGING_H
#define PAGING_H

#include "lib.h"
#include "x86_desc.h"
#include "syscall_handle.h"
#include "process.h"

/**
  Sets up the environment, page directories etc and
  enables paging.
**/
void initialize_paging();

/**
  Causes the specified page directory to be loaded into the
  CR3 register. 
**/
void enable_page();

/**
  Handler for page faults.
**/
void page_fault ();

/** 
  Set the value in a pde
**/
void setpde(uint32_t index, uint32_t value);

uint32_t getpde(uint32_t index);

void video_map_4kb(uint32_t p_addr, uint32_t v_addr, uint32_t idx);

#endif
